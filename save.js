/// <reference path="./config.d.ts" />
if(!window.puggan_save_stage)
{
	window.puggan_save_stage = {
		cell: 0,
		current_bones: 0,
		current_nature: 0,
		de: 0,
		he: 0, // game.global.totalHeliumEarned
		mi: 0,
		pf: "",
		portal: 0,
		world: 0,
		zones: 0, // game.stats.zonesCleared.value + game.stats.zonesCleared.valueTotal,
	};
}

puggan_save = () => {
	if(!PlayFab || !PlayFab._internalSettings || !PlayFab._internalSettings.sessionTicket)
	{
		return;
	}
	const new_save_stage = {
		cell: 1 + game.global.lastClearedCell,
		current_bones: game.global.b,
		current_nature: game.empowerments.Ice.tokens + game.empowerments.Poison.tokens + game.empowerments.Wind.tokens,
		de: game.global.essence + game.global.spentEssence,
		he: game.global.totalHeliumEarned,
		mi: game.global.magmite,
		pf: PlayFab._internalSettings.sessionTicket.replace(/-.*/, ""),
		portal: game.global.totalPortals,
		world: game.global.world,
		zones: game.stats.zonesCleared.value + game.stats.zonesCleared.valueTotal,
	};
	if(puggan_save_stage.he >= new_save_stage.he && puggan_save_stage.portal >= new_save_stage.portal && puggan_save_stage.world >= new_save_stage.world)
	{
		return;
	}
	window.puggan_save_stage = new_save_stage;
	new_save_stage.save = save(true);
	if(window.voidmap) {
		new_save_stage.voidmap = voidmap();
	}
	const ajax = new XMLHttpRequest();
	ajax.open("POST", "https://trimps.puggan.se/trimps/save.php");
	ajax.send(JSON.stringify(new_save_stage));
};

if(puggan_auto_trimps.timers.save)
{
	clearInterval(puggan_auto_trimps.timers.save);
}

puggan_auto_trimps.timers.save = setInterval(puggan_save, 3000);
