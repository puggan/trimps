/* tslint:disable:no-namespace */
/// <reference path="decimal.js/decimal.global.d.ts" />

declare namespace trimps
{
	interface Achievement {
		breakpoints: string[];
		description: () => string;
		finished: number|boolean[];
		icon: string;
		names: string[];
		newStuff: any[];
		tiers: number[];
		title: string;
	}

	interface Achievements {
		angerTimed: Achievement;
		bionicTimed: Achievement;
		blockTimed: Achievement;
		dailyHelium: Achievement;
		damage: Achievement;
		doomTimed: Achievement;
		heliumHour: Achievement;
		housing: Achievement;
		humaneRun: Achievement;
		oneOffs: Achievement;
		portals: Achievement;
		prisonTimed: Achievement;
		spire2Timed: Achievement;
		spire3Timed: Achievement;
		spire4Timed: Achievement;
		spireTimed: Achievement;
		starTimed: Achievement;
		totalGems: Achievement;
		totalHeirlooms: Achievement;
		totalHelium: Achievement;
		totalMaps: Achievement;
		totalZones: Achievement;
		trimps: Achievement;
		wallTimed: Achievement;
		zones: Achievement;
	}

	interface BadGuy {
		attack: number;
		fast: boolean;
		health: number;
		location: "None"|"World"|"All"|"Sea"|"Mountain"|"Forest"|"Depths"|"Plentiful"|"Void"|"Hell"|"Block"|"Wall"|"Doom"|"Prison"|"Bionic"|"Star"|"Maps";
		loot: (level) => void;
		world: number;
	}

	interface BadGuys {
		Artimp: BadGuy;
		Autoimp: BadGuy;
		Blimp: BadGuy;
		Brickimp: BadGuy;
		Carbimp: BadGuy;
		Chickimp: BadGuy;
		Chimp: BadGuy;
		Chronoimp: BadGuy;
		Cthulimp: BadGuy;
		Destructimp: BadGuy;
		Dragimp: BadGuy;
		Elephimp: BadGuy;
		Entimp: BadGuy;
		Feyimp: BadGuy;
		Flowimp: BadGuy;
		Flutimp: BadGuy;
		Frimp: BadGuy;
		Fusimp: BadGuy;
		Gnomimp: BadGuy;
		Goblimp: BadGuy;
		Golimp: BadGuy;
		Gorillimp: BadGuy;
		Gravelimp: BadGuy;
		Grimp: BadGuy;
		Hippopotamimp: BadGuy;
		Hulking_Mutimp: BadGuy;
		Hydrogimp: BadGuy;
		Improbability: BadGuy;
		Indianimp: BadGuy;
		Jestimp: BadGuy;
		Kangarimp: BadGuy;
		Kittimp: BadGuy;
		Lavimp: BadGuy;
		Liquimp: BadGuy;
		Magnimp: BadGuy;
		Mechimp: BadGuy;
		Megablimp: BadGuy;
		Megaskeletimp: BadGuy;
		Mitschimp: BadGuy;
		Moltimp: BadGuy;
		Mountimp: BadGuy;
		Mutimp: BadGuy;
		Neutrimp: BadGuy;
		Omnipotrimp: BadGuy;
		Onoudidimp: BadGuy;
		Penguimp: BadGuy;
		Presimpt: BadGuy;
		Pumpkimp: BadGuy;
		Robotrimp: BadGuy;
		Seirimp: BadGuy;
		Shadimp: BadGuy;
		Shrimp: BadGuy;
		Skeletimp: BadGuy;
		Slagimp: BadGuy;
		Slosnimp: BadGuy;
		Snimp: BadGuy;
		Squimp: BadGuy;
		Squirrimp: BadGuy;
		Tauntimp: BadGuy;
		Terminatimp: BadGuy;
		Titimp: BadGuy;
		Turkimp: BadGuy;
		Turtlimp: BadGuy;
		Venimp: BadGuy;
		Voidsnimp: BadGuy;
		Warden: BadGuy;
		Whipimp: BadGuy;
	}

	interface Building
	{
		alert: boolean;
		cost: Costs;
		craftTime: number;
		increase: BuildingIncrease;
		locked: number;
		owned: number;
		percent: boolean;
		purchased: number;
		tooltip: string;
	}

	interface Buildings
	{
		Barn: Building;
		Collector: Building;
		Forge: Building;
		Gateway: Building;
		Gym: Building;
		Hotel: Building;
		House: Building;
		Hut: Building;
		Mansion: Building;
		Nursery: Building;
		Resort: Building;
		Shed: Building;
		Trap: Building;
		Tribute: Building;
		Warpstation: Building;
		Wormhole: Building;
	}

	interface BuildingIncrease
	{
		what: string;
		by: number;
	}

	type building_name =
		"Barn" |
		"Collector" |
		"Forge" |
		"Gateway" |
		"Gym" |
		"Hotel" |
		"House" |
		"Hut" |
		"Mansion" |
		"Nursery" |
		"Resort" |
		"Shed" |
		"Trap" |
		"Tribute" |
		"Warpstation" |
		"Wormhole";

	interface Cost
	{
		0: number;
		1: number;
		2: number;
	}
	interface Costs
	{
		"metal": Cost;
	}

	type cost_name =
		"metal" |
		"gems";

	interface Empowerment
	{
		baseModifier: number;
		color: string;
		currentDebuffPower: number;
		description: any;
		formatModifier: any;
		getCombatModifier: any;
		getModifier: any;
		level: number;
		maxStacks: number;
		retainLevel: number;
		tokens: number;
		upgradeDescription: any;
	}

	interface Empowerments {
		Poison: Empowerment;
		Wind: Empowerment;
		Ice: Empowerment;
	}

	interface Equipment
	{
		locked: boolean;
		tooltip: string;
		modifier: number;
		level: number;
		cost: Costs;
		oc: number;
		health: number;
		healthCalculated: number;
		prestige: number;
	}

	interface Equipments
	{
		Arbalest: Equipment;
		Battleaxe: Equipment;
		Boots: Equipment;
		Breastplate: Equipment;
		Dagger: Equipment;
		Gambeson: Equipment;
		Greatsword: Equipment;
		Helmet: Equipment;
		Mace: Equipment;
		Pants: Equipment;
		Polearm: Equipment;
		Shield: Equipment;
		Shoulderguards: Equipment;
	}

	type equipment_name =
		"Battleaxe" |
		"Boots" |
		"Breastplate" |
		"Dagger" |
		"Greatsword" |
		"Helmet" |
		"Mace" |
		"Pants" |
		"Polearm" |
		"Shield" |
		"Shoulderguards";

	interface Fluffy
	{
		abortPrestige: any;
		baseExp: any;
		calculateExp: any;
		calculateInfo: any;
		calculateLevel: any;
		canGainExp: any;
		checkAndRunVoidance: any;
		checkAndRunVoidelicious: any;
		currentExp: any;
		currentLevel: any;
		damageModifiers: any;
		expBreakdown: any;
		expGrowth: any;
		firstLevel: any;
		getBonusForLevel: any;
		getDamageModifier: () => number;
		getExp: any;
		getExpReward: any;
		getFirstLevel: any;
		getFluff: any;
		getLevel: any;
		getMinZoneForExp: any;
		getVoidStackCount: any;
		growth: any;
		handleBox: any;
		isActive: () => boolean;
		isMaxLevel: any;
		isRewardActive: any;
		prestige: any;
		prestigeDamageModifier: any;
		prestigeExpModifier: any;
		prestigeRewards: any;
		refreshTooltip: any;
		rewardConfig: any;
		rewardExp: any;
		rewards: any;
		specialExpModifier: any;
		specialModifierReason: any;
		tooltip: any;
		updateExp: any;
	}

	interface Global
	{
		achievementBonus: any;
		addonUser: any;
		antiStacks: any;
		attack: any;
		autoBattle: any;
		autoCraftModifier: any;
		autoGolden: any;
		autoJobsSetting: any;
		autoPrestiges: any;
		autoSave: any;
		autoStorage: any;
		autoStorageAvailable: any;
		autoStructureSetting: any;
		autoUpgrades: any;
		autoUpgradesAvailable: any;
		b: any;
		battleCounter: any;
		bestFluffyExp: any;
		bestHelium: any;
		bestTokens: any;
		betaV: any;
		bionicOwned: any;
		block: any;
		bonePortalThisRun: any;
		breedBack: any;
		breedTime: any;
		brokenPlanet: any;
		buildingsQueue: any;
		buyAmt: any;
		buyTab: any;
		canMagma: any;
		canMapAtZone: any;
		canRespecPerks: any;
		canScryCache: any;
		capTrimp: any;
		challengeActive: any;
		crafting: any;
		currentMapId: any;
		dailyChallenge: any;
		dailyHelium: any;
		decayDone: any;
		difs: any;
		eggLoc: any;
		eggSeed: any;
		enemySeed: any;
		essence: any;
		fighting: any;
		firing: any;
		firstCustomAmt: any;
		firstCustomExact: any;
		fluffyExp: any;
		fluffyPrestige: any;
		formation: any;
		freeTalentRespecs: any;
		freshFight: any;
		frugalDone: any;
		generatorMode: any;
		Geneticistassist: any;
		GeneticistassistSetting: any;
		GeneticistassistSteps: any;
		genPaused: any;
		genStateConfig: any;
		getEnemyAttack: (level: number, name: string|null, ignoreImpStat:boolean) => number;
		getEnemyHealth: (level: number, name: string|null, ignoreImpStat:boolean) => number;
		goldenUpgrades: any;
		gridArray: any;
		health: any;
		heirloomBoneSeed: any;
		heirloomsCarried: any;
		heirloomSeed: any;
		heirloomsExtra: any;
		heliumLeftover: any;
		hideMapRow: any;
		highestLevelCleared: any;
		holidaySeed: any;
		improvedAutoStorage: any;
		isBeta: any;
		justAmalged: any;
		killSavesBelow: any;
		kongBonusMode: any;
		lastBonePresimpt: any;
		lastBreedTime: any;
		lastClearedCell: any;
		lastClearedMapCell: any;
		lastCustomAmt: any;
		lastCustomExact: any;
		lastFightUpdate: any;
		lastLowGen: any;
		lastOfflineProgress: any;
		lastOnline: any;
		lastPortal: any;
		lastSkeletimp: any;
		lastSoldierSentAt: any;
		lastSpireCleared: any;
		lastUnlock: any;
		lastVoidMap: any;
		lastWarp: any;
		lockTooltip: any;
		lookingAtMap: any;
		lootAvgs: any;
		lowestGen: any;
		magmaFuel: any;
		magmite: any;
		mapBonus: any;
		mapExtraBonus: any;
		mapGridArray: any;
		mapPresets: any;
		mapsActive: any;
		mapsOwned: any;
		mapsOwnedArray: any;
		mapStarted: any;
		mapsUnlocked: any;
		maxCarriedHeirlooms: any;
		maxSoldiersAtStart: any;
		maxSplit: any;
		menu: any;
		messages: any;
		mutationSeed: any;
		nextQueueId: any;
		nullifium: any;
		numTab: any;
		passive: any;
		pauseFight: any;
		perkPreset1: any;
		perkPreset2: any;
		perkPreset3: any;
		playerGathering: any;
		playerModifier: any;
		playFabLoginType: any;
		portalActive: any;
		portalTime: any;
		pp: any;
		preMapsActive: any;
		presimptStore: any;
		prestige: any;
		prisonClear: any;
		realBreedTime: any;
		recentDailies: any;
		rememberInfo: any;
		removingPerks: any;
		repeatMap: any;
		researched: any;
		respecActive: any;
		roboTrimpCooldown: any;
		roboTrimpLevel: any;
		runFluffyExp: any;
		runningChallengeSquared: any;
		runTokens: any;
		scrySeed: any;
		selectedChallenge: any;
		selectedHeirloom: any;
		selectedMapPreset: any;
		ShieldEquipped: any;
		skeleSeed: any;
		sLevel: any;
		slowDone: any;
		soldierCurrentAttack: any;
		soldierCurrentBlock: any;
		soldierHealth: any;
		soldierHealthMax: any;
		soldierHealthRemaining: any;
		spentEssence: any;
		spireActive: any;
		spireDeaths: any;
		spireRows: any;
		spiresCompleted: any;
		spreadsheetMode: any;
		StaffEquipped: any;
		start: any;
		statsMode: any;
		stringVersion: any;
		sugarRush: any;
		supervisionSetting: any;
		switchToMaps: any;
		switchToWorld: any;
		tab: any;
		tempHighHelium: any;
		time: any;
		timeLeftOnCraft: any;
		timeLeftOnTrap: any;
		timeSinceLastGeneratorTick: any;
		titimpLeft: any;
		totalGifts: any;
		totalHeliumEarned: any;
		totalMapsEarned: any;
		totalPortals: any;
		totalSquaredReward: any;
		totalVoidMaps: any;
		trapBuildAllowed: any;
		trapBuildToggled: any;
		trimpsGenerated: any;
		turkimpTimer: any;
		useShriek: any;
		usingShriek: any;
		version: any;
		viewingUpgrades: any;
		voidBuff: any;
		voidDeaths: any;
		voidMapsToggled: any;
		voidMaxLevel: any;
		voidSeed: any;
		waitToScry: any;
		waitToScryMaps: any;
		world: number;
		zoneStarted: any;
	}

	interface Job
	{
		alert: any;
		allowAutoFire: boolean;
		cost: Costs;
		getBonusPercent?: (justStacks?: boolean, forceTime?: number) => number;
		increase: string;
		locked: number;
		modifier: number;
		owned: number;
		tooltip: string;
	}

	interface Jobs
	{
		Amalgamator: Job;
		Dragimp: Job;
		Explorer: Job;
		Farmer: Job;
		Geneticist: Job;
		Lumberjack: Job;
		Magmamancer: Job;
		Miner: Job;
		Scientist: Job;
		Trainer: Job;
	}

	type job_name =
		"Amalgamator" |
		"Dragimp" |
		"Explorer" |
		"Farmer" |
		"Geneticist" |
		"Lumberjack" |
		"Magmamancer" |
		"Miner" |
		"Scientist" |
		"Trainer";

	interface Mutation
	{
		active: any;
		attack?: any;
		cellCount?: () => number;
		change?: any;
		checkDirection?: any;
		checkDuplicates?: any;
		effects?: any;
		getLastArray?: any;
		health?: any;
		namePrefix?: any;
		nextMove?: any;
		pattern: any;
		randomStart?: any;
		reward?: any;
		savePattern?: any;
		statScale?: any;
		tooltip?: any;
		updateGrid?: any;
	}

	interface Mutations
	{
		Living: Mutation;
		Corruption: Mutation;
		Magma: Mutation;
		Healthy: Mutation;
	}

	interface Perk
	{
		heliumSpent: any;
		level: any;
		modifier: any;
		priceBase: any;
		tooltip: any;
	}

	interface Portal
	{
		Agility: any;
		Anticipation: any;
		Artisanistry: any;
		Bait: any;
		Capable: any;
		Carpentry: any;
		Carpentry_II: any;
		Classy: any;
		Coordinated: any;
		Cunning: any;
		Curious: any;
		Looting: any;
		Looting_II: any;
		Meditation: any;
		Motivation: any;
		Motivation_II: any;
		Overkill: any;
		Packrat: Perk;
		Pheromones: any;
		Power: any;
		Power_II: any;
		Range: any;
		Relentlessness: any;
		Resilience: any;
		Resourceful: any;
		Siphonology: any;
		Toughness: any;
		Toughness_II: any;
		Trumps: any;
	}

	interface Resource
	{
		max: number;
		owned: number;
	}

	interface HeliumResource extends Resource
	{
		respecMax: number;
	}

	interface TrimpsResource extends Resource
	{
		employed: any;
		getCurrentSend: (checkLevelTemp?: any) => number;
		maxMod: number;
		maxSoldiers: number;
		potency: number;
		realMax: () => number;
		soldiers: number;
		speed: number;
		working: number;
	}

	interface Resources
	{
		food: Resource;
		fragments: Resource;
		gems: Resource;
		helium: HeliumResource;
		metal: Resource;
		science: Resource;
		trimps: TrimpsResource;
		wood: Resource;
	}

	type resource_name =
		"food" |
		"fragments" |
		"gems" |
		"helium" |
		"metal" |
		"science" |
		"trimps" |
		"wood";

	type resource_gathering_name =
		"buildings" |
		"food" |
		"metal" |
		"science" |
		"trimps" |
		"wood";

	interface Talent
	{
		description: string;
		name: string;
		tier: number;
		purchased: boolean;
		icon: string;
		requires?: string;
	}

	interface Talents {
		amalg: Talent;
		autoJobs: Talent;
		autoStructure: Talent;
		bionic2: Talent;
		bionic: Talent;
		blacksmith2: Talent;
		blacksmith3: Talent;
		blacksmith: Talent;
		bounty: Talent;
		crit: Talent;
		daily: Talent;
		deciBuild: Talent;
		doubleBuild: Talent;
		explorers2: Talent;
		explorers: Talent;
		fluffyAbility: Talent;
		fluffyExp: Talent;
		foreman: Talent;
		headstart2: Talent;
		headstart3: Talent;
		headstart: Talent;
		healthStrength: Talent;
		housing: Talent;
		hyperspeed2: Talent;
		hyperspeed: Talent;
		liquification2: Talent;
		liquification: Talent;
		magmaFlow: Talent;
		magmamancer: Talent;
		mapLoot2: Talent;
		mapLoot: Talent;
		nature2: Talent;
		nature3: Talent;
		nature: Talent;
		overkill: Talent;
		patience: Talent;
		pierce: Talent;
		portal: Talent;
		quickGen: Talent;
		scry2: Talent;
		scry: Talent;
		skeletimp2: Talent;
		skeletimp: Talent;
		stillRowing2: Talent;
		stillRowing: Talent;
		turkimp2: Talent;
		turkimp3: Talent;
		turkimp4: Talent;
		turkimp: Talent;
		voidPower2: Talent;
		voidPower3: Talent;
		voidPower: Talent;
		voidSpecial2: Talent;
		voidSpecial: Talent;
	}

	interface Upgrade
	{
		alert: any;
		allowed: any;
		cost: any;
		done: any;
		fire: any;
		locked: any;
		modifier: any;
		tooltip: any;
	}

	interface Upgrades
	{
		Anger: Upgrade;
		Axeidic: Upgrade;
		Barrier: Upgrade;
		Battle: Upgrade;
		Bestplate: Upgrade;
		Blockmaster: Upgrade;
		Bloodlust: Upgrade;
		Bootboost: Upgrade;
		Bounty: Upgrade;
		Coordination: Upgrade;
		Dagadder: Upgrade;
		Dominance: Upgrade;
		Efficiency: Upgrade;
		Egg: Upgrade;
		Explorers: Upgrade;
		Formations: Upgrade;
		GambesOP: Upgrade;
		Gigastation: Upgrade;
		Greatersword: Upgrade;
		Gymystic: Upgrade;
		Harmbalest: Upgrade;
		Hellishmet: Upgrade;
		Magmamancers: Upgrade;
		Megafarming: Upgrade;
		Megalumber: Upgrade;
		Megamace: Upgrade;
		Megaminer: Upgrade;
		Megascience: Upgrade;
		Miners: Upgrade;
		Pantastic: Upgrade;
		Polierarm: Upgrade;
		Potency: Upgrade;
		Scientists: Upgrade;
		Shieldblock: Upgrade;
		Smoldershoulder: Upgrade;
		Speedexplorer: Upgrade;
		Speedfarming: Upgrade;
		Speedlumber: Upgrade;
		Speedminer: Upgrade;
		Speedscience: Upgrade;
		Supershield: Upgrade;
		SuperShriek: Upgrade;
		Trainers: Upgrade;
		TrainTacular: Upgrade;
		Trapstorm: Upgrade;
		UberHotel: Upgrade;
		UberHouse: Upgrade;
		UberHut: Upgrade;
		UberMansion: Upgrade;
		UberResort: Upgrade;
	}

	type upgrade_name =
		"Anger" |
		"Axeidic" |
		"Barrier" |
		"Battle" |
		"Bestplate" |
		"Blockmaster" |
		"Bloodlust" |
		"Bootboost" |
		"Bounty" |
		"Coordination" |
		"Dagadder" |
		"Dominance" |
		"Efficiency" |
		"Egg" |
		"Explorers" |
		"Formations" |
		"GambesOP" |
		"Gigastation" |
		"Greatersword" |
		"Gymystic" |
		"Harmbalest" |
		"Hellishmet" |
		"Magmamancers" |
		"Megafarming" |
		"Megalumber" |
		"Megamace" |
		"Megaminer" |
		"Megascience" |
		"Miners" |
		"Pantastic" |
		"Polierarm" |
		"Potency" |
		"Scientists" |
		"Shieldblock" |
		"Smoldershoulder" |
		"Speedexplorer" |
		"Speedfarming" |
		"Speedlumber" |
		"Speedminer" |
		"Speedscience" |
		"Supershield" |
		"SuperShriek" |
		"Trainers" |
		"TrainTacular" |
		"Trapstorm" |
		"UberHotel" |
		"UberHouse" |
		"UberHut" |
		"UberMansion" |
		"UberResort";

	interface Game
	{
		achievements: any;
		badGuyDeathTexts: any;
		badGuys: BadGuys;
		buildings: Buildings;
		c2: any;
		challenges: any;
		empowerments: Empowerments;
		equipment: Equipments;
		generatorUpgrades: any;
		global: Global;
		goldenUpgrades: any;
		heirlooms: any;
		jobs: Jobs;
		mapConfig: any;
		mapUnlocks: any;
		options: any;
		permanentGeneratorUpgrades: any;
		portal: Portal;
		resources: Resources;
		settings: any;
		singleRunBonuses: any;
		stats: any;
		talents: Talents;
		tierValues: any;
		triggers: any;
		trimpDeathTexts: any;
		unlocks: any;
		upgrades: Upgrades;
		workspaces: any;
		worldText: any;
		worldUnlocks: any;
	}

	interface PlayFabSettings
	{
		sessionTicket: string;
	}
	interface PlayFab
	{
		_internalSettings: PlayFabSettings;
	}
}

// noinspection JSUnusedGlobalSymbols the global var window is a Window
declare interface Window {
	auto_timer: number;
	Fluffy: trimps.Fluffy;
	game: trimps.Game;
	mutations: trimps.Mutations;
}
export declare class DecimalBreed extends Decimal {}

declare function abandonChallenge(restart: any): any;

declare function abandonDaily(): any;

declare function activateClicked(): any;

declare function activateKongBonus(oldWorld: any): any;

declare function activatePortal(): any;

declare function activateShriek(): any;

declare function activateTurkimpPowers(): any;

declare function addAvg(what: any, number: any): any;

declare function addBoost(level: any, previewOnly: any): any;

declare function addCarried(confirmed: any): any;

declare function addGeneticist(amount: any): any;

declare function addHelium(amt: any): any;

declare function addMapModifier(location: any, modifier, clear: any): any;

declare function addMaxHousing(amt: any, giveTrimps: any): any;

declare function addNewFeats(indexArray: any): any;

declare function addNewSetting(name: any): any;

declare function addQueueItem(what: any): any;

declare function addResCheckMax(what: any, number, noStat, fromGather, nonFilteredLoot: any): any;

declare function addSpecials(maps: any, countOnly, map, getPrestiges: any): any;

declare function addSpecialToLast(special: any, array, item: any): any;

declare function addSpecialToNthLast(special: any, array, item, n: any): any;

declare function addToBundle(what: any): any;

declare function addTooltipPricing(toTip: any, what, isItIn: any): any;

declare function addVoidAlert(): any;

declare function adjustMap(what: any, value: any): any;

declare function adjustMessageIndexes(index: any): any;

declare function affordMaxLevelsCheap(levels: any): any;

declare function affordMaxLevelsPerfect(levels: any): any;

declare function affordOneTier(what: any, whereFrom, take: any): any;

declare function applyS1(): any;

declare function applyS2(): any;

declare function applyS3(): any;

declare function applyS5(): any;

declare function assignExtraWorkers(): any;

declare function autoBuyJob(what: any, isRatio, purchaseAmt, max: any): any;

declare function autoBuyUpgrade(item: any): any;

declare function autoGoldenUpgrades(): any;

declare function autoPrestiges(equipmentAvailable: any): any;

declare function autoSave(): any;

declare function autoStorage(): any;

declare function autoTrap(): any;

declare function autoUnlockHousing(): any;

declare function autoUpgrades(): any;

declare function battleCoordinator(makeUp: any): any;

declare function battle(force: any): any;

declare function boostHe(checkOnly: any): any;

declare function breed(): any;

declare function buffVoidMaps(): any;

declare function buildBuilding(what: any): any;

declare function buildGrid(): any;

declare function buildMapGrid(mapId: any): any;

declare function buildModOptionDdl(type: any, rarity, selectedMod: any): any;

declare function buildNiceCheckbox(id: any, extraClass, enabled: any): any;

declare function buyAutoJobs(allowRatios: any): any;

declare function buyAutoStructures(): any;

declare function buyBuilding(what: trimps.building_name, confirmed?: boolean, fromAuto?: boolean, forceAmt?: boolean|number): void;

declare function buyEquipment(what: trimps.equipment_name, confirmed?: boolean, noTip?: boolean): void;

declare function buyGeneratorUpgrade(item: any): any;

declare function buyGoldenUpgrade(what: "Battle"|"Helium"|"Void"): boolean;

declare function buyJob(what: trimps.job_name, confirmed: boolean, noTip: boolean): void;

declare function buyMap(): any;

declare function buyPermanentGeneratorUpgrade(item: any): any;

declare function buyPortalUpgrade(what: any): any;

declare function buyUpgrade(what: trimps.upgrade_name, confirmed?: boolean, noTip?: boolean, heldCtrl?: boolean): void;

declare function cacheReward(resourceName: any, time, cacheName: any): any;

declare function calcHeirloomBonusDecimal(type: any, name, number, getValueOnly: any): any;

declare function calcHeirloomBonus(type: string, name: string, number: number, getValueOnly?: boolean): number;

declare function calculateDamage(number: any, buildString, isTrimp, noCheckAchieve, cell: any): any;

declare function calculateMaxAfford(itemObj: trimps.Building, isBuilding: true, isEquipment: false, isJob: false, forceMax: boolean, forceRatio: number);
declare function calculateMaxAfford(itemObj: trimps.Equipment, isBuilding: false, isEquipment: true, isJob: false, forceMax: boolean, forceRatio: number);
declare function calculateMaxAfford(itemObj: trimps.Job, isBuilding: false, isEquipment: false, isJob: true, forceMax: boolean, forceRatio: number);

declare function calculatePercentageBuildingCost(what: any, resourceToCheck, costModifier, replaceMax: any): any;

declare function calculateScryingReward(): any;

declare function calculateTimeToMax(resource: any, perSec, toNumber, fromGather: any): any;

declare function canAffordBuilding(what: trimps.building_name | trimps.equipment_name, take?: boolean, buildCostString?: boolean, isEquipment?: boolean, updatingLabel?: boolean, forceAmt?: number, autoPerc?: number): boolean;

declare function canAffordCoordinationTrimps(): boolean;

declare function canAffordGeneratorUpgrade(): any;

declare function canAffordJob(what: any, take, workspaces, updatingLabel, fromAuto: any): any;

declare function canAffordTwoLevel(whatObj: trimps.Upgrade, takeEm: boolean): boolean;

declare function cancelPlayFab(): any;

declare function cancelPortal(keep: any): any;

declare function cancelTooltip(ignore2: any): any;

declare function canCommitCarpentry(): any;

declare function canGeneratorTick(): any;

declare function canPurchaseRow(tierNumber: any): any;

declare function capitalizeFirstLetter(word: any): any;

declare function carryHeirloom(): any;

declare function changeGeneratorState(to: any, updateOnly: any): any;

declare function checkAdvMaps2(hidden: any): any;

declare function checkAffordableTalents(): any;

declare function checkAlert(what: any, isItIn: any): any;

declare function checkAmalgamate(): any;

declare function checkAndFormatTokens(tokenCost: any, empowerment: any): any;

declare function checkBundleForImp(what: any, justHighlight: any): any;

declare function checkButtons(what: any): any;

declare function checkChallengeSquaredAllowed(): any;

declare function checkCompleteDailies(): any;

declare function checkCrushedCrit(): any;

declare function checkEndOfQueue(): any;

declare function checkGenStateSwitch(): any;

declare function checkHandleResourcefulRespec(): any;

declare function checkHousing(getHighest: any): any;

declare function checkIfLiquidZone(): any;

declare function checkIfSpireWorld(getNumber: any): any;

declare function checkJobItem(what: any, take, costItem, amtOnly, toBuy: any): any;

declare function checkLowestHeirloom(): any;

declare function checkMapLevelInput(elem: any): any;

declare function checkMaxSliders(): any;

declare function checkModCap(mod: any, modConfig, heirloom: any): any;

declare function checkOfflineProgress(noTip: any): any;

declare function checkPerfectChecked(): any;

declare function checkSelectedModsFor(what: any): any;

declare function checkSlidersForPerfect(): any;

declare function checkTriggers(force: any): any;

declare function checkVoidMap(): any;

declare function clearMapDescription(): any;

declare function clearNewSettings(): any;

declare function clearPerks(): any;

declare function clearQueue(specific: any): any;

declare function clearSettingTabs(): any;

declare function clearSpireMetals(): any;

declare function closeHeirPopup(): any;

declare function commitPortalUpgrades(usingPortal: any): any;

declare function compareVersion(compareTo: any, compare: any): any;

declare function completeTalentPurchase(talent: any): any;

declare function configMessages(): any;

declare function confirmAbandonChallenge(): any;

declare function convertNotationsToNumber(num: any): any;

declare function convertUnlockIconToSpan(special: any): any;

declare function costUpdatesTimeout(): any;

declare function countAlertsIn(where: any): any;

declare function countChallengeSquaredReward(): any;

declare function countDailyWeight(dailyObj: any): any;

declare function countHeliumSpent(checkTemp: any): any;

declare function countPriceOfUpgrades(dummyHeirloom: any, count: any): any;

declare function countPurchasedTalents(tier: any): any;

declare function countRemainingEssenceDrops(): any;

declare function countStackedVoidMaps(): any;

declare function countUnpurchasedImports(): any;

declare function craftBuildings(makeUp: any): any;

declare function createHeirloom(zone: any, fromBones: any): any;

declare function createMap(newLevel: any, nameOverride, locationOverride, lootOverride, sizeOverride, difficultyOverride, setNoRecycle, messageOverride: any): any;

declare function createVoidMap(forcePrefix: any, forceSuffix, skipMessage: any): any;

declare function curateAvgs(): any;

declare function customizeGATargets(): any;

declare function dayOfWeek(number: any): any;

declare function deadInSpire(): any;

declare function decayNurseries(): any;

declare function disableShriek(): any;

declare function displayAddCarriedBtn(): any;

declare function displayAllSettings(): any;

declare function displayAllStats(buildAll: any): any;

declare function displayCarriedHeirlooms(): any;

declare function displayChallenges(): any;

declare function displayExtraHeirlooms(): any;

declare function displayGoldenUpgrades(redraw: any): any;

declare function displayNature(): any;

declare function displayPerksBtn(): any;

declare function displayPortalUpgrades(fromTab: any): any;

declare function displayRoboTrimp(): any;

declare function displaySelectedHeirloom(modSelected: any, selectedIndex, fromTooltip, locationOvr, indexOvr, fromPopup: any): any;

declare function displaySingleRunBonuses(): any;

declare function displayTalents(): any;

declare function distributeToChallenges(amt: any): any;

declare function drawAllBuildings(): any;

declare function drawAllEquipment(): any;

declare function drawAllJobs(): any;

declare function drawAllUpgrades(): any;

declare function drawBuilding(what: any, where: any): any;

declare function drawEquipment(what: any, elem: any): any;

declare function drawGeneticistassist(where: any): any;

declare function drawGrid(maps: any): any;

declare function drawJob(what: any, where: any): any;

declare function drawUpgrade(what: any, where: any): any;

declare function dropPrestiges(): any;

declare function easterEggClicked(): any;

declare function enableDisableTab(what: any, enable: any): any;

declare function enableImprovedAutoStorage(): any;

declare function enablePerkConfirmBtn(): any;

declare function enablePlayFab(): any;

declare function endSpire(cancelEarly: any): any;

declare function equipHeirloom(noScreenUpdate: any): any;

declare function escapeRegExp(str: any): any;

declare function everythingInArrayGreaterEqual(smaller: any, bigger: any): any;

declare function exportPerks(): any;

declare function fadeIn(elem: any, speed: any): any;

declare function fight(makeUp: any): any;

declare function fightManual(): any;

declare function filterMessage(what: any, updateOnly: any): any;

declare function filterTabs(what: any): any;

declare function findHomeForSpecial(special: any, item, array, max: any): any;

declare function fireMode(noChange: any): any;

declare function formatDailySeedDate(): any;

declare function formatMinutesForDescriptions(number: any): any;

declare function formatMultAsPercent(mult: any): any;

declare function formatSecondsForDescriptions(number: any): any;

declare function freeWorkspace(amount: any, getAmtFreed: any): any;

declare function gameLoop(makeUp: any, now: any): any;

declare function gameTimeout(): any;

declare function gather(): any;

declare function generateHeirloomIcon(heirloom: any, location, number: any): any;

declare function generatorTick(fromOverclock: any): any;

declare function getAdditivePrice(atLevel: any, portalUpgrade: any): any;

declare function getAllowedTalentTiers(): any;

declare function getAmountInRange(maxRange: any, toKeep: any);

declare function getAvailableGoldenUpgrades(): number;

declare function getAvgLootSecond(what: any): any;

declare function getBadCoordLevel(): any;

declare function getBarColorClass(percent: any): any;

declare function getBaseBlock(): any;

declare function getBattleStatBd(what: any): any;

declare function getBuildingItemPrice(toBuy: trimps.Building, costItem: trimps.cost_name, isEquipment: false, purchaseAmt: number): any;
declare function getBuildingItemPrice(toBuy: trimps.Equipment, costItem: trimps.cost_name, isEquipment: true, purchaseAmt: number): any;

declare function getChallengeSquaredButtonColor(challenge: any): any;

declare function getCheapestPrestigeUpgrade(upgradeArray: any): any;

declare function getCraftTime(buildingObj: any): any;

declare function getCurrentChallengePane(): any;

declare function getCurrentDailyDescription(): any;

declare function getCurrentMapCell(): any;

declare function getCurrentMapObject(): any;

declare function getCurrentTime(): any;

declare function getCurrentWorldCell(): any;

declare function getDailyChallenge(add: any, objectOnly, textOnly: any): any;

declare function getDailyHeliumValue(weight: any): any;

declare function getDailyTimeString(add: any, makePretty, getDayOfWeek: any): any;

declare function getDailyTopText(add: any): any;

declare function getDesiredGenes(ovr: any): any;

declare function getEmpowerment(adjust: any, getNaming: any): any;

declare function getExtraMapLevels(): any;

declare function getFluctuation(number: any, minFluct, maxFluct: any): any;

declare function getFuelBurnRate(): any;

declare function getGameTime(): any;

declare function getGeneratorFuelCap(includeStorage: any, checkingHybrid: any): any;

declare function getGeneratorHtml(getContainer: any): any;

declare function getGeneratorTickAmount(): any;

declare function getGeneratorTickTime(): any;

declare function getGeneratorUpgradeHtml(): any;

declare function getGenStateConfigBtnText(num: any): any;

declare function getGenStateConfigTooltip(): any;

declare function getGoldenFrequency(fluffTier: any): any;

declare function getHeirloomRarityRanges(zone: any): any;

declare function getHeirloomRarity(zone: any, seed: any): any;

declare function getHeirloomZoneBreakpoint(zone: any): any;

declare function getHighestIdealRow(): any;

declare function getHighestPrestige(): any;

declare function getHighestPurchaseableRow(): any;

declare function getHighestTalentTier(): any;

declare function getHighestUnlockedTalentTier(): any;

declare function getHousingMultiplier(): any;

declare function getIndividualSquaredReward(challengeName: any, forceHighest: any): any;

declare function getLootBd(what: any): any;

declare function getMagmiteDecayAmt(): any;

declare function getMagmiteReward(): any;

declare function getMapBiomeSetting(): any;

declare function getMapIcon(mapObject: any, nameOnly: any): any;

declare function getMapIndex(mapId: any): any;

declare function getMapMinMax(what: any, value: any): any;

declare function getMapPreset(): any;

declare function getMapSliderValue(what: any): any;

declare function getMapSpecTag(modifier: any): any;

declare function getMapZoneOffset(): any;

declare function getMaxForResource(what: any): any;

declare function getMaxResources(what: any): any;

declare function getMaxTrimps(): any;

declare function getMegaCritDamageMult(critTier: any): any;

declare function getMinutesThisPortal(): any;

declare function getModReplaceCost(heirloom: any, mod: any): any;

declare function getModUpgradeCost(heirloom: any, modIndex, count: any): any;

declare function getModUpgradeValue(heirloom: any, modIndex, count: any): any;

declare function getNextCarriedCost(): any;

declare function getNextGeneticistCost(): any;

declare function getNextNatureCost(empowerment: any, forRetain: any): any;

declare function getNextPrestigeCost(what: any): any;

declare function getNextPrestigeValue(what: any): any;

declare function getNextTalentCost(forceAmt: any): any;

declare function getNextVoidId(): any;

declare function getPerkBuyCount(perkName: any): any;

declare function getPierceAmt(): number;

declare function getPlayerCritChance(): any;

declare function getPlayerCritDamageMult(): any;

declare function getPlayerModifier(): any;

declare function getPlayFabLoginHTML(): any;

declare function getPortalUpgradePrice(what: any, removing, forceAmt: any): any;

declare function getPsString(what: trimps.resource_name, rawNum?: false): string;
declare function getPsString(what: trimps.resource_name, rawNum: true): number;

declare function getQueueElemIndex(id: any, queue: any): any;

declare function getRandomBadGuy(mapSuffix: any, level, totalCells, world, imports, mutation, visualMutation: any): any;

declare function getRandomBySteps(steps: any, mod, fromBones: any): any;

declare function getRandomIntSeeded(seed: any, minIncl, maxExcl: any): any;

declare function getRandomMapName(): any;

declare function getRandomMapValue(what: any): any;

declare function getRecycleValue(level: any): any;

declare function getRetainModifier(empowermentName: any): any;

declare function getScientistInfo(number: any, reward: any): any;

declare function getScientistLevel(): any;

declare function getSeededRandomFromArray(seed: any, array: any): any;

declare function getSelectedHeirloom(locationOvr: any, indexOvr: any): any;

declare function getSettingHtml(optionItem: any, item, forceClass, appendId: any): any;

declare function getSpecialModifierSetting(): any;

declare function getSpecialSquaredRewards(challenge: any): any;

declare function getSpireStats(cellNum: any, name, what: any): any;

declare function getSpireStory(spireNum: any, row: any): any;

declare function getSquaredDescriptionInRun(hideDesc: any): any;

declare function getStepPriceIncrease(heirloom: any, mod, add: any): any;

declare function getTabClass(displayed: any): any;

declare function getTooltipJobText(what: any, toBuy: any): any;

declare function getTotalHeirloomRefundValue(heirloom: any): any;

declare function getTotalTalentCost(): any;

declare function getTrimpPs(): any;

declare function getUniqueColor(item: any): any;

declare function getWarpstationColor(): any;

declare function getZoneStats(event: any, update: any): any;

declare function giveHeliumReward(mod: any): any;

declare function givePresimptLoot(): any;

declare function givePumpkimpLoot(): any;

declare function giveSpireReward(level: any): any;

declare function goRadial(elem: any, currentSeconds, totalSeconds, frameTime: any): any;

declare function handleDominationDebuff(): any;

declare function handleExitSpireBtn(): any;

declare function handleFinishDailyBtn(): any;

declare function handleIceDebuff(): any;

declare function handlePauseMessage(send: any): any;

declare function handlePoisonDebuff(): any;

declare function handleWindDebuff(): any;

declare function hideAdvMaps(displayOnly: any, hideForVoid: any): any;

declare function hideBones(): any;

declare function hideFormations(): any;

declare function hideHeirloomSelectButtons(): any;

declare function hidePurchaseBones(): any;

declare function htmlEncode(text: any): any;

declare function importPerks(): any;

declare function increaseTheHeat(): any;

declare function incrementMapLevel(amt: any): any;

declare function initializeInputText(): any;

declare function initTalents(): any;

declare function isNumberBad(number: any): any;

declare function isObjectEmpty(obj: any): any;

declare function isScryerBonusActive(): any;

declare function kredPurchase(what: any): any;

declare function levelEquipment(what: any, manualNumber: any): any;

declare function liquifyZone(): any;

declare function loadEquipment(oldEquipment: any): any;

declare function loadFromPlayFab(): any;

declare function loadFromPlayFabCallback(data: any, error: any): any;

declare function loadGigastations(): any;

declare function loadPerkPreset(): any;

declare function load(saveString: any, autoLoad, fromPf: any): any;

declare function loadSingleBonusColors(): any;

declare function log10(val: any): any;

declare function magnetoShriek(): any;

declare function manageLeadStacks(remove: any): any;

declare function mapLevelHotkey(up: any): any;

declare function mapsClicked(confirmed: any): any;

declare function mapsSwitch(updateOnly: any, fromRecycle: any): any;

declare function messageConfigHover(what: any, event: any): any;

declare function messageMapCredits(): any;

declare function message(messageString: any, type, lootIcon, extraClass, extraTag, htmlPrefix: any): any;

declare function naturePurchase(doing: any, spending, convertTo: any): any;

declare function natureTooltip(event: any, doing, spending, convertTo: any): any;

declare function needAnS(number: any): any;

declare function nextWorld(): any;

declare function nodeToArray(nodeList: any): any;

declare function numTab(what: any, p: any): any;

declare function onPurchaseResult(result: any): any;

declare function pauseFight(updateOnly: any): any;

declare function pauseGenerator(updateOnly: any): any;

declare function planetBreaker(): any;

declare function playFabAttemptReconnect(reconnected: any): any;

declare function playFabFinishLogin(downloadFirst: any): any;

declare function playFabLoginCallback(data: any, error: any): any;

declare function playFabLoginWithKongregate(attempt: any): any;

declare function playFabLoginWithPlayFab(username: any, pass: any): any;

declare function playFabRecoverCallback(data: any, error: any): any;

declare function playFabRecoverInfo(needsPassword: any): any;

declare function playFabRegisterPlayFabUser(): any;

declare function playFabSaveCheck(): any;

declare function playFabSaveCheckCallback(data: any, error: any): any;

declare function populateHeirloomWindow(): any;

declare function populateSpecialModifiers(): any;

declare function portalClicked(): any;

declare function positionTooltip(elem: any, event, extraInf: any): any;

declare function postMessages(): any;

declare function presetTab(tabNum: any): any;

declare function prestigeEquipment(what: any, fromLoad, noInc: any): any;

declare function prettify(number: number): string|number;

declare function prettifySub(number: number): string|number;

declare function preventZoom(elem: any): any;

declare function purchaseBoost(num: any): any;

declare function purchaseBundle(): any;

declare function purchaseBundleClicked(): any;

declare function purchaseImport(): any;

declare function purchaseMisc(what: any): any;

declare function purchaseSingleRunBonus(what: any): any;

declare function purchaseTalentRow(tier: any): any;

declare function purchaseTalent(what: any): any;

declare function purgeBionics(): any;

declare function readNiceCheckbox(elem: any): any;

declare function readPlayFabInfo(): any;

declare function recalculateHeirloomBonuses(): any;

declare function recycleAllExtraHeirlooms(valueOnly: any): any;

declare function recycleAllHeirloomsClicked(confirmed: any): any;

declare function recycleBelow(confirmed: any): any;

declare function recycleHeirloom(confirmed: any): any;

declare function recycleMap(map: any, fromMass, killVoid: any): any;

declare function reevaluateTimedAchieve(achieveName: any): any;

declare function refreshGenStateConfigTooltip(): any;

declare function refreshMaps(): any;

declare function refundQueueItem(what: any): any;

declare function removeGeneticist(amount: any): any;

declare function removeGoldenUpgrades(): any;

declare function removePerk(what: any): any;

declare function removeQueueItem(what: any, force: any): any;

declare function renameHeirloom(cancel: any, fromPopup: any): any;

declare function renamePerkPreset(needTooltip: any, name: any): any;

declare function repeatClicked(updateOnly: any): any;

declare function replaceAll(str: any, find, replace: any): any;

declare function replaceMod(confirmed: any): any;

declare function resetAdvMaps(fromClick: any): any;

declare function resetEmpowerStacks(): any;

declare function resetGame(keepPortal: any): any;

declare function resetPresets(): any;

declare function resetSingleBonusColors(): any;

declare function resolvePow(cost: any, whatObj, addOwned: any): any;

declare function respecPerks(fromPortal: any): any;

declare function respecTalents(confirmed: any, force: any): any;

declare function restoreGrid(): any;

declare function restoreNumTab(): any;

declare function rewardLiquidZone(): any;

declare function rewardResource(what: any, baseAmt, level, checkMapLootScale, givePercentage: any): any;

declare function rewardSpire1(level: any): any;

declare function rewardToken(empowerment: any): any;

declare function romanNumeral(number: any): any;

declare function runEverySecond(makeUp: any): any;

declare function runGameLoop(makeUp: any, now: any): any;

declare function runMap(): any;

declare function saveAdvMaps(): any;

declare function saveAutoJobsConfig(): any;

declare function saveAutoStructureConfig(): any;

declare function save(exportThis: true, fromManual: boolean): string;
declare function save(exportThis: false, fromManual: boolean): void;

declare function saveGenStateConfig(): any;

declare function saveMapAtZone(): any;

declare function savePerkPreset(): any;

declare function saveSupervisionSetting(): any;

declare function saveToPlayFabCallback(data: any, error: any): any;

declare function saveToPlayFab(saveString: any): any;

declare function scaleLootLevel(level: any, mapLevel: any): any;

declare function scaleNumberForBonusHousing(num: any): any;

declare function scaleToCurrentMap(amt: any, ignoreBonuses, ignoreScry: any): any;

declare function searchSettings(elem: any): any;

declare function seededRandom(seed: any): any;

declare function selectAdvMapsPreset(num: any): any;

declare function selectBoost(num: any): any;

declare function selectChallenge(what: any): any;

declare function selectHeirloom(number: any, location, noScreenUpdate: any): any;

declare function selectImp(name: any): any;

declare function selectMap(mapId: any, force: any): any;

declare function selectMod(which: any, fromPopup: any): any;

declare function setAdvExtraZoneText(): any;

declare function setEmpowerTab(): any;

declare function setFormation(what: number | "0"): void;

declare function setGatherTextAs(what: any, on: any): any;

declare function setGather(what: trimps.resource_gathering_name, updateOnly: boolean): void;

declare function setHeirRareText(forBones: any): any;

declare function setMax(amount: any, forPortal: any): any;

declare function setMutationTooltip(which: any, mutation: any): any;

declare function setNewCraftItem(): any;

declare function setNonMapBox(): any;

declare function settingTab(what: any): any;

declare function setupDummyHeirloom(heirloom: any, mod: any): any;

declare function setVoidBuffTooltip(): any;

declare function setVoidCorruptionIcon(regularMap: any): any;

declare function showBones(): any;

declare function showGeneratorUpgradeInfo(item: any, permanent: any): any;

declare function showPurchaseBones(): any;

declare function simpleSeconds(what: any, seconds: any): any;

declare function stackPoison(trimpAttack: any): any;

declare function startBundling(): any;

declare function startDaily(): any;

declare function startFight(): any;

declare function startQueue(what: any, count: any): any;

declare function startSpire(confirmed: any): any;

declare function startTheMagma(): any;

declare function stopCarryHeirloom(): any;

declare function storePlayFabInfo(name: any, pass: any): any;

declare function successPurchaseFlavor(): any;

declare function swapClass(prefix: any, newClass, elem: any): any;

declare function swapFromPopup(): any;

declare function swapNiceCheckbox(elem: any, forceSetting: any): any;

declare function swapNotation(updateOnly: any): any;

declare function swapToCurrentChallenge(updateOnly: any): any;

declare function switchForm(register: any): any;

declare function switchFormToRecovery(): any;

declare function testAllWeights(): any;

declare function testDailySpread(): any;

declare function testGymystic(oldPercent: any): any;

declare function timeToDegrees(currentSeconds: any, totalSeconds: any): any;

declare function toggleAutoGolden(noChange: any): any;

declare function toggleAutoJobsHelp(): any;

declare function toggleAutoJobs(noChange: any, forceOff: any): any;

declare function toggleAutoPrestiges(noChange: any): any;

declare function toggleAutoStorage(noChange: any): any;

declare function toggleAutoStructure(noChange: any, forceOff: any): any;

declare function toggleAutoTrap(updateOnly: any): any;

declare function toggleAutoUpgrades(noChange: any): any;

declare function toggleChallengeSquared(): any;

declare function toggleGeneticistassist(updateOnly: any): any;

declare function toggleGenStateConfig(elem: any, num: any): any;

declare function toggleHeirloomHelp(): any;

declare function toggleHeirlooms(): any;

declare function toggleMapGridHtml(on: any, currentMapObj: any): any;

declare function toggleMinusRes(on: any): any;

declare function toggleRemovePerks(noUpdate: any): any;

declare function toggleSettingAlert(): any;

declare function toggleSettingSection(toSearch: any): any;

declare function toggleSetting(setting: any, elem, fromPortal, updateOnly, backwards: any): any;

declare function toggleSettingsMenu(): any;

declare function toggleStats(toggleMode: any): any;

declare function toggleVoidMaps(updateOnly: any): any;

declare function tooltip(what: any, isItIn, event, textString, attachFunction, numCheck, renameBtn, noHide, hideCancel, ignoreShift: any): any;

declare function toZalgo(string: any, seed, strength: any): any;

declare function trapThings(): any;

declare function trimMessages(what: any): any;

declare function tryPlayFabAutoLogin(): any;

declare function tryScry(): any;

declare function unequipHeirloom(heirloom: any, toLocation, noScreenUpdate: any): any;

declare function unlockAutoGolden(): any;

declare function unlockBuilding(what: any): any;

declare function unlockEquipment(what: any, fromCheck: any): any;

declare function unlockFormation(what: any): any;

declare function unlockJob(what: any): any;

declare function unlockMapStuff(): any;

declare function unlockMap(what: any): any;

declare function unlockTooltip(): any;

declare function unlockUpgrade(what: any, displayOnly: any): any;

declare function updateAllBattleNumbers(skipNum: any): any;

declare function updateAllPerkColors(): any;

declare function updateAntiStacks(): any;

declare function updateBadBar(cell: any): any;

declare function updateBalanceStacks(): any;

declare function updateBoneBtnColors(): any;

declare function updateBones(): any;

declare function updateBuildSpeed(): any;

declare function updateButtonColor(what: any, canAfford, isJob: any): any;

declare function updateDailyClock(justTime: any): any;

declare function updateDailyStacks(what: any): any;

declare function updateDecayStacks(addStack: any): any;

declare function updateElectricityStacks(tipOnly: any): any;

declare function updateElectricityTip(): any;

declare function updateEmpowerCosts(): any;

declare function updateExtraLevelColors(): any;

declare function updateForemenCount(): any;

declare function updateGeneratorFuel(): any;

declare function updateGeneratorInfo(): any;

declare function updateGeneratorUpgradeHtml(): any;

declare function updateGoodBar(): any;

declare function updateImportButton(text: any, enabled: any): any;

declare function updateImports(which: any): any;

declare function updateLabels(): any;

declare function updateLivingStacks(): any;

declare function updateMapCost(getValue: any, forceBaseCost: any): any;

declare function updateMapCredits(): any;

declare function updateMapNumbers(): any;

declare function updateNatureInfoSpans(): any;

declare function updateNextGeneratorTickTime(): any;

declare function updateNomStacks(number: any): any;

declare function updatePauseBtn(show: any): any;

declare function updatePerkColor(what: any): any;

declare function updatePerkLevel(what: any): any;

declare function updatePortalChallengeAbandonButton(): any;

declare function updatePortalTimer(justGetTime: any): any;

declare function updatePresetColor(): any;

declare function updatePs(jobObj: any, trimps, jobName: any): any;

declare function updateSideTrimps(): any;

declare function updateSkeleBtn(): any;

declare function updateStoredGenInfo(breeding: any): any;

declare function updateTalentNumbers(): any;

declare function updateTitimp(): any;

declare function updateToxicityStacks(): any;

declare function updateTurkimpTime(): any;

declare function upgradeMod(confirmed: any, count: any): any;

declare function validateJobRatios(): any;

declare function verticalCenterTooltip(makeLarge: any, makeSuperLarge: any): any;

declare function viewPortalUpgrades(): any;

declare function zoomShortcut(e: any): any;

declare var PlayFab: trimps.PlayFab;
