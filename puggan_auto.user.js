/* tslint:disable:no-console */
/// <reference path="./game.d.ts" />
/// <reference path="./puggan.v1.d.ts" />
/// <reference path="./config.d.ts" />

/**
 * @param {number} low
 * @param {number} value
 * @param {number} high
 * @returns {number}
 */
range = (low, value, high) => Math.floor(Math.min(Math.max(low, value), high));
storage_max = (resource_name) => calcHeirloomBonus("Shield", "storageSize", ((game.resources[resource_name].max * (1 + game.portal.Packrat.modifier * game.portal.Packrat.level))));
storage_total_time = (resource_name) => storage_max(resource_name) / getPsString(resource_name, true);
storage_time_to_max = (resource_name) => (storage_max(resource_name) - game.resources[resource_name].owned) / getPsString(resource_name, true);

puggan = () => {
	const amount = game.global.buyAmt;
	const job_fire_mode = game.global.firing;
	const build_gymystic = (game.upgrades.Gymystic.allowed === 1 && game.upgrades.Gymystic.done === 0);
	const config = puggan_auto_trimps.config;
	game.global.buyAmt = 1;

	const max_trimps = game.resources.trimps.realMax();
	let breeders = Math.floor(game.resources.trimps.owned - game.resources.trimps.employed);
	let freeWorkers = Math.min(breeders - 2, breeders - Math.ceil(max_trimps / 5), Math.ceil(max_trimps / 2) - game.resources.trimps.employed);

	// No Breeders needed when running Trapper, just reserv a unit of soldiers, employ the rest.
	const TrapperSend = game.global.challengeActive === "Trapper" ? game.resources.trimps.getCurrentSend() : 0;
	if(TrapperSend)
	{
		freeWorkers = Math.min(breeders - TrapperSend, Math.ceil(max_trimps / 2) - game.resources.trimps.employed);
	}

		/** @type {EquipmentDescriptions[]} equipment_descriptions */
	const equipment_descriptions = [
		{
			equipment_name: "Arbalest",
			equipment_type: "Weapon",
			upgrade_name: "Harmbalest",
			upgrade_offset: 5,
		},
		{
			equipment_name: "Greatsword",
			equipment_type: "Weapon",
			upgrade_name: "Greatersword",
			upgrade_offset: 5,
		},
		{
			equipment_name: "Battleaxe",
			equipment_type: "Weapon",
			upgrade_name: "Axeidic",
			upgrade_offset: 4,
		},
		{
			equipment_name: "Polearm",
			equipment_type: "Weapon",
			upgrade_name: "Polierarm",
			upgrade_offset: 3,
		},
		{
			equipment_name: "Mace",
			equipment_type: "Weapon",
			upgrade_name: "Megamace",
			upgrade_offset: 2,
		},
		{
			equipment_name: "Dagger",
			equipment_type: "Weapon",
			upgrade_name: "Dagadder",
			upgrade_offset: 1,
		},
		{
			equipment_name: "Gambeson",
			equipment_type: "Armor",
			upgrade_name: "GambesOP",
			upgrade_offset: 5,
		},
		{
			equipment_name: "Breastplate",
			equipment_type: "Armor",
			upgrade_name: "Bestplate",
			upgrade_offset: 5,
		},
		{
			equipment_name: "Shoulderguards",
			equipment_type: "Armor",
			upgrade_name: "Smoldershoulder",
			upgrade_offset: 4,
		},
		{
			equipment_name: "Pants",
			equipment_type: "Armor",
			upgrade_name: "Pantastic",
			upgrade_offset: 3,
		},
		{
			equipment_name: "Helmet",
			equipment_type: "Armor",
			upgrade_name: "Hellishmet",
			upgrade_offset: 2,
		},
		{
			equipment_name: "Boots",
			equipment_type: "Armor",
			upgrade_name: "Bootboost",
			upgrade_offset: 1,
		},
	];

	//<editor-fold desc="Storage">
	if(!game.buildings.Forge.locked && (game.buildings.Forge.owned < config.storage_min_count || storage_time_to_max("metal") < config.storage_min_time) && canAffordBuilding("Forge"))
	{
		buyBuilding("Forge", false, true);
	}
	if(!game.buildings.Shed.locked && (game.buildings.Shed.owned < config.storage_min_count || storage_time_to_max("wood") < config.storage_min_time) && canAffordBuilding("Shed"))
	{
		buyBuilding("Shed", false, true);
	}
	if(!game.buildings.Barn.locked && (game.buildings.Barn.owned < config.storage_min_count || storage_time_to_max("food") < config.storage_min_time) && canAffordBuilding("Barn"))
	{
		buyBuilding("Barn", false, true);
	}
	//</editor-fold>

	//<editor-fold desc="Upgrades">
	/** @type {trimps.upgrade_name[]} */
	const upgradeList = [
		"Miners",
		"Scientists",
		"Gymystic",
		"Coordination",
		"Speedminer",
		"Speedlumber",
		"Speedfarming",
		"Speedscience",
		"Speedexplorer",
		"Megaminer",
		"Megalumber",
		"Megafarming",
		"Megascience",
		"Efficiency",
		"TrainTacular",
		"Trainers",
		"Explorers",
		"Blockmaster",
		"Battle",
		"Bloodlust",
		"Bounty",
		"Egg",
		"Anger",
		"Formations",
		"Dominance",
		"Barrier",
		"UberHut",
		"UberHouse",
		"UberMansion",
		"UberHotel",
		"UberResort",
		"Trapstorm",
		// 'Gigastation',
		"Potency",
		"Magmamancers",
		//"Supershield",
	];

	/**
	 * @param {trimps.upgrade_name} upgrade_name
	 * @returns {boolean}
	 */
	const try_upgrade = upgrade_name => {
		const upgrade = game.upgrades[upgrade_name];
		if(upgrade.allowed <= upgrade.done)
		{
			return false;
		}
		if(!canAffordTwoLevel(upgrade))
		{
			return true;
		}
		if(upgrade_name === "Coordination" && !canAffordCoordinationTrimps())
		{
			return false;
		}
		message("Upgraded " + upgrade_name + " to " + (1 + upgrade.done), "Notices", "paperclip", "puggan_auto_notice");
		buyUpgrade(upgrade_name, false, true);
		return false;
	};

	if(getAvailableGoldenUpgrades() > 0)
	{
		if(game.global.runningChallengeSquared || config.prio_gold_battle)
		{
			buyGoldenUpgrade("Battle");
		}
		else
		{
			buyGoldenUpgrade("Helium");
		}
	}

	if(game.upgrades.Scientists.allowed)
	{
		for(const upgrade_name of upgradeList)
		{
			if(upgrade_name === "Potency")
			{
				if(game.global.challengeActive === "Trapper")
				{
					continue;
				}
				if(freeWorkers > 0)
				{
					continue;
				}
				// if no Geneticistassist, try to calculate a good time to upgrade
				if(game.portal.Anticipation.level && (game.jobs.Geneticist.locked || !game.global.Geneticistassist))
				{
					const breedtime = +document.getElementById("trimpsTimeToFill")
						.innerText
						.replace(/.*Secs . /, "")
						.replace(/ Secs/, "");
					// 30s + 10% = 33s
					if(breedtime <= config.potency_max_time)
					{
						continue;
					}

					console.log("Potency breedtime: " + breedtime);
					continue;
				}
			}

			if(upgrade_name === "Coordination" && game.global.challengeActive === "Trapper")
			{
				continue;
			}
			try_upgrade(upgrade_name);
		}
	}
	else
	{
		try_upgrade("Battle");
		try_upgrade("Miners");
		if(!game.global.challengeActive || config.auto_fight)
		{
			try_upgrade("Bloodlust");
		}
	}
	//</editor-fold>

	//<editor-fold desc="Equipmeny">
	/** @type {trimps.equipment_name[]} */

	// if(game.upgrades.Scientists.allowed) {
	// let no_equipment_upgrades = !game.global.mapsActive;

	/**
	 * @param {trimps.equipment_name} equipment_name
	 * @param {number} max_level
	 */
	const try_equipment = (equipment_name, max_level, expensive) => {
		const equipment = game.equipment[equipment_name];
		if(equipment.locked)
		{
			return;
		}
		if(equipment.level >= max_level)
		{
			return;
		}
		if(!canAffordBuilding(equipment_name, false, false, true))
		{
			return;
		}
		if(equipment_name === "Shield") {
			// todo
		}
		else if(!expensive || game.upgrades.Gigastation.done < game.upgrades.Gigastation.allowed && game.upgrades.Coordination.done < game.upgrades.Coordination.allowed)
		{
			const price = parseFloat(getBuildingItemPrice(game.equipment[equipment_name], "metal", true, 1));
			if(price > 0.5 * game.resources.metal.owned)
			{
				return;
			}
		}
		message("Increased Equipment " + equipment_name + " to " + (1 + equipment.level), "Notices", "paperclip", "puggan_auto_notice");
		buyEquipment(equipment_name, false, true);
	};

	let upgrades_to_pickup = 0;
	let upgrades_to_buy = 0;
	let no_armor_upgrades = false;
	let max_armor_level = config.max_equipment;
	let lowest_weapon_level = -1;
	let artisanistry_modifier = Math.pow(1 - game.portal.Artisanistry.modifier, game.portal.Artisanistry.level);

	if(game.upgrades.Miners.allowed > game.upgrades.Miners.done)
	{
		upgrades_to_pickup = -1;
		upgrades_to_buy = -1;
	}
	else
	{
		for(const equipment_description of equipment_descriptions)
		{
			const upgrade = game.upgrades[equipment_description.upgrade_name];
			const expected_upgrade_level = Math.floor((game.global.world - equipment_description.upgrade_offset) / 5);
			upgrades_to_pickup += Math.max(0, expected_upgrade_level - upgrade.allowed);
			upgrades_to_buy += Math.max(0, upgrade.allowed - upgrade.done);
		}

		puggan_auto_trimps.info(config.info_rows.equipment, "U: " + upgrades_to_buy + " + " + upgrades_to_pickup);

		for(const equipment_description of equipment_descriptions)
		{
			const upgrade = game.upgrades[equipment_description.upgrade_name];
			const equipment = game.equipment[equipment_description.equipment_name];
			const expected_upgrade_level = Math.floor((game.global.world - equipment_description.upgrade_offset) / 5);

			if(equipment.locked)
			{
				continue;
			}

			let max_upgrade_level = upgrade.allowed;
			if(no_armor_upgrades && equipment_description.equipment_type !== "Weapon")
			{
				if(upgrade.done >= lowest_weapon_level - 2)
				{
					max_upgrade_level = upgrade.done + 1;
					if(upgrade.cost.resources.metal * artisanistry_modifier > game.resources.metal.owned * 0.05)
					{
						//console.log([equipment_description.upgrade_name, upgrade.cost.resources.metal / game.resources.metal.owned, upgrade.cost.resources.metal, game.resources.metal.owned]);
						continue;
					}
				}
				else if(max_upgrade_level > lowest_weapon_level - 2)
				{
					max_upgrade_level = lowest_weapon_level - 2;
				}
			}

			if(game.upgrades.Scientists.allowed)
			{
				if(max_upgrade_level > upgrade.done)
				{
					let missing_upgrades = max_upgrade_level - upgrade.done;
					while(missing_upgrades--)
					{
						if(!canAffordTwoLevel(upgrade))
						{
							break;
						}
						try_upgrade(equipment_description.upgrade_name);
					}
				}

				if(upgrade.allowed > upgrade.done || expected_upgrade_level > upgrade.done)
				{
					if(equipment_description.equipment_type === "Weapon")
					{
						no_armor_upgrades = true;
						if(lowest_weapon_level < 0)
						{
							lowest_weapon_level = upgrade.done;
						}
						else if(lowest_weapon_level > upgrade.done)
						{
							lowest_weapon_level = upgrade.done;
						}
					}
					continue;
				}
			}

			if(equipment.level >= config.max_equipment)
			{
				continue;
			}

			if(equipment_description.equipment_type === "Weapon")
			{
				max_armor_level = Math.min(max_armor_level, equipment.level);
				try_equipment(equipment_description.equipment_name, config.max_equipment, upgrades_to_buy < 1);
			}
			else
			{
				try_equipment(equipment_description.equipment_name, max_armor_level, upgrades_to_buy < 1);
			}
		}
	}

	if(!build_gymystic)
	{
		if(!game.upgrades.Scientists.allowed || !try_upgrade("Supershield"))
		{
			try_equipment("Shield", game.global.mapsActive ? config.max_map_equipment : config.max_equipment_shield, true);
		}
	}
	//</editor-fold>

	//<editor-fold desc="Jobs">
	/**
	 * @param {trimps.job_name} job
	 * @param {number|"Max"} count
	 */
	const employ = (job, count) => {
		if(!game.jobs[job])
		{
			return;
		}
		if(game.jobs[job].locked)
		{
			return;
		}
		game.global.firing = false;
		game.global.buyAmt = count;
		buyJob(job, false, true);
		game.global.buyAmt = 1;
		game.global.firing = job_fire_mode;
	};

	// Special jobs (Explorers, Trainers, Genetics)
	if(breeders > 2 && config.auto_employ)
	{
		if(!game.jobs.Explorer.locked)
		{
			const max_explorer = config.max_explorer - game.jobs.Explorer.owned;
			const count_explorer = range(0, calculateMaxAfford(game.jobs.Explorer, false, false, true, false, 1), max_explorer);
			if(count_explorer > 0)
			{
				employ("Explorer", count_explorer);
				freeWorkers -= count_explorer;
				breeders -= count_explorer;
			}
		}
		if(!game.jobs.Trainer.locked)
		{
			const avaible_workers = Math.min(breeders, freeWorkers + game.resources.trimps.employed - game.jobs.Trainer.owned);
			const count_trainers = range(0, calculateMaxAfford(game.jobs.Trainer, false, false, true, false, 1), avaible_workers);
			if(count_trainers > 0)
			{
				employ("Trainer", count_trainers);
				freeWorkers -= count_trainers;
				breeders -= count_trainers;
			}
		}
		if(!game.jobs.Geneticist.locked && !game.global.Geneticistassist)
		{
			const breedtime = +document.getElementById("trimpsTimeToFill")
				.innerText
				.replace(/.*Secs . /, "")
				.replace(/ Secs/, "");
			// 30s + 1% = 30.3s
			if(breedtime < config.geneticist_min_time)
			{
				employ("Geneticist", 1);
				freeWorkers--;
				breeders--;
			}
		}
	}

	// Normal jobs
	if(breeders > 2 && config.auto_employ)
	{
		// Unlock Lumberjack
		if(game.jobs.Lumberjack.locked && !game.jobs.Farmer.owned)
		{
			employ("Farmer", 1);
			freeWorkers--;
		}

		// At least one worker on each job
		if(freeWorkers <= 0)
		{
			/** @type {trimps.job_name} job_name */
			for(let job_name of ["Farmer", "Lumberjack", "Miner", "Scientist"])
			{
				if(game.jobs[job_name].locked)
				{
					continue;
				}
				if(game.jobs[job_name].owned)
				{
					continue;
				}
				employ(job_name, 1);
			}
		}
		else
		{
			const plans = [
				[1, 1, 1, 1], //   4
				[200, 200, 200, 100], // 700
				[4e3, 4e3, 4e3, 1e3], //  13k
				[40e3, 40e3, 40e3, 2e3], // 122k
				[100e3, 100e3, 100e3, 5e3], // 305k
				[1e6, 1e6, 2e6, 50e3], //   4.05M
				[10e6, 10e6, 100e6, 1e6], // 121M
				[1e12, 1e12, 10e12, 1e6], // 12T
				[1e60, 1e60, 10e60, 1e6], // 12e60
			];

			const f = game.jobs.Farmer.owned;
			const l = game.jobs.Lumberjack.locked ? 10 * max_trimps : game.jobs.Lumberjack.owned;
			const m = game.jobs.Miner.locked ? 10 * max_trimps : game.jobs.Miner.owned;
			const s = game.jobs.Scientist.locked ? 10 * max_trimps : game.jobs.Scientist.owned;

			for(const plan of plans)
			{
				if(plan[0] <= f && plan[1] <= l && plan[2] <= m && plan[3] <= s)
				{
					continue;
				}

				const jobs = {
					f: {
						c: f,
						g: plan[0],
						n: "Farmer",
						r: f / plan[0],
					},
					l: {
						c: l,
						g: plan[1],
						n: "Lumberjack",
						r: l / plan[1],
					},
					m: {
						c: m,
						g: plan[2],
						n: "Miner",
						r: m / plan[2],
					},
					s: {
						c: s,
						g: plan[3],
						n: "Scientist",
						r: s / plan[3],
					},
				};

				let employed = 0;
				let goal_employees = 0;
				let min_ration = 1;
				for(const current_job_key in jobs)
				{
					if(!jobs.hasOwnProperty(current_job_key))
					{
						continue;
					}
					const current_job = jobs[current_job_key];
					if(current_job.c >= current_job.g)
					{
						continue;
					}
					employed += current_job.c;
					goal_employees += current_job.g;
					min_ration = Math.min(min_ration, current_job.r);
				}

				const goal_ratio = (employed + freeWorkers) / goal_employees;

				for(const current_job_key in jobs)
				{
					if(!jobs.hasOwnProperty(current_job_key))
					{
						continue;
					}
					const current_job = jobs[current_job_key];
					if(current_job.c >= current_job.g || current_job.r > min_ration)
					{
						continue;
					}

					const job_goal_employees = range(current_job.c, Math.ceil(current_job.g * goal_ratio), current_job.g);
					const missing_employees = Math.ceil(job_goal_employees - current_job.c);
					const afford_jobs = calculateMaxAfford(game.jobs[current_job.n], false, false, true);
					if(missing_employees > 0 && afford_jobs > 0)
					{
						employ(current_job.n, Math.min(afford_jobs, missing_employees));
						break;
					}
				}

				break;
			}
		}
	}
	//</editor-fold>

	/**
	 * @param {trimps.building_name} building_name
	 * @param {number?} building_count
	 * @returns {boolean}
	 */
	const try_building = (building_name, building_count) => {
		if(!canAffordBuilding(building_name))
		{
			return false;
		}
		const building = game.buildings[building_name];
		if(building.locked)
		{
			return false;
		}
		if(building_count > 1)
		{
			const building_max_count = calculateMaxAfford(game.buildings[building_name], true, false);
			if(building_max_count < building_count)
			{
				building_count = building_max_count;
			}
		}
		const lvl_before = building.owned;
		buyBuilding(building_name, false, true, building_count || 1);
		const lvl_after = building.owned;
		const lvl_delta = lvl_after - lvl_before;
		if(lvl_delta > 0 && building_name !== "Tribute")
		{
			if(lvl_delta === 1)
			{
				message("Building " + building_name + " " + lvl_after, "Notices", "paperclip", "puggan_auto_notice");
			}
			else
			{
				message("Building " + building_name + " x" + lvl_delta + " (" + lvl_before + " -> " + lvl_after + ")", "Notices", "paperclip", "puggan_auto_notice");
			}
		}
		return true;
	};

	//<editor-fold desc="Buildings">
	if(game.global.buildingsQueue.length < config.max_build_queue)
	{
		const building_in_queue = game.global.buildingsQueue.length === 1 ? game.global.buildingsQueue[0].replace(/\.\d+/, "") : "";
		/** @type {trimps.building_name[]} */
		const buildingList = [
			"Warpstation",
			"Collector",
			"Gateway",
			"Resort",
			"Hotel",
			"Mansion",
			"House",
			"Hut",
			"Gym",
			"Tribute",
			"Nursery",
		];
		let building_count = 1;

		building_loop:
			for(const building_name of buildingList)
			{
				if(building_in_queue === building_name)
				{
					break;
				}
				switch(building_name)
				{
					case "Warpstation":
						const max_warpsations = config.gigastation_init + config.gigastation_delta * game.upgrades.Gigastation.done;
						const warpstations_before = game.buildings[building_name].owned;
						building_count = max_warpsations - warpstations_before;
						if(building_count <= 0)
						{
							const gigastation_before = game.upgrades.Gigastation.done;
							if(gigastation_before < 39)
							{
								try_upgrade("Gigastation");
								if(game.upgrades.Gigastation.done > gigastation_before)
								{
									const warpstations_after = game.buildings[building_name].owned;
									message("Building " + building_name + " (" + warpstations_before + " -> 0 -> " + warpstations_after + ")", "Notices", "paperclip", "puggan_auto_notice");
									break building_loop;
								}
								continue;
							}
							else if(game.global.world >= 230)
							{
								continue;
							}
							else
							{
								building_count = 100;
							}
						}
						break;

					case "Gym":
						if(build_gymystic)
						{
							continue;
						}

						let gym_max_count = game.buildings.Gym.owned + calculateMaxAfford(game.buildings[building_name], true, false);

						if(game.upgrades.Supershield.allowed > game.upgrades.Supershield.done || game.equipment.Shield.level < config.prio_shield_until)
						{
							const rounded_down_gym_count = 10 * (Math.floor(gym_max_count / 10) - 1);
							gym_max_count = range(config.prio_gym_until, rounded_down_gym_count, gym_max_count);
						}

						building_count = gym_max_count - game.buildings.Gym.owned;
						if(building_count < 1)
						{
							continue;
						}
						break;

					case "Nursery":
						// No auto Nursery in Magma
						if(game.global.world >= 230)
						{
							continue;
						}
						if(game.upgrades.Potency.allowed > game.upgrades.Potency.done)
						{
							continue;
						}
						if(game.global.challengeActive === "Trapper")
						{
							continue;
						}
						if(game.portal.Anticipation.level && (game.jobs.Geneticist.locked || !game.global.Geneticistassist))
						{
							continue;
						}

						const nursery_overflow = game.buildings.Nursery.owned % 50;
						const nursery_max_count = calculateMaxAfford(game.buildings.Nursery, true, false);
						if(nursery_max_count + nursery_overflow < 100)
						{
							continue;
						}

						building_count = 50 * Math.floor((nursery_overflow + nursery_max_count) / 50 - 1) - nursery_overflow;
						// No auto Nursery in Magma
						if(game.global.highestLevelCleared > 232)
						{
							if(game.buildings.Nursery.owned >= config.max_nursery)
							{
								continue;
							}
							if(game.buildings.Nursery.owned + building_count >= config.max_nursery)
							{
								building_count = config.max_nursery - game.buildings.Nursery.owned;
							}
						}
						break;

					case "Collector":
						if(!game.buildings.Warpstation.locked)
						{
							const c_ratio = getBuildingItemPrice(game.buildings.Collector, "gems", false, 1) / game.buildings.Collector.increase.by;
							const w_ratio = getBuildingItemPrice(game.buildings.Warpstation, "gems", false, 1) / game.buildings.Warpstation.increase.by;
							if(w_ratio < c_ratio)
							{
								continue;
							}
							building_count = 10 - (game.buildings.Collector.owned % 10);
						}
						break;

					case "Tribute":
						building_count = 100;
						break;

					default:
						if(game.buildings[building_name].owned >= config.max_houses)
						{
							continue;
						}
						building_count = config.max_houses - game.buildings[building_name].owned;
						break;
				}

				if(try_building(building_name, building_count))
				{
					break;
				}
			}
	}
	//</editor-fold>

	//<editor-fold desc="PlayerGathering">
	let PlayerGatheringSugestin = null;
	// Trapper challenge
	if(game.global.challengeActive === "Trapper" && document.getElementById("trimpsBar").style.width !== "100%")
	{
		const trapable_trimps = game.buildings.Trap.owned * (1 + game.portal.Bait.level);
		const missing_trimps = max_trimps - game.resources.trimps.owned;
		// enough traps to fill to max
		if(trapable_trimps > missing_trimps && missing_trimps > 1)
		{
			PlayerGatheringSugestin = "trimps";
		}
		// enough traps to fill to soldiers-limit
		if(breeders < TrapperSend && TrapperSend <= breeders + trapable_trimps)
		{
			PlayerGatheringSugestin = "trimps";
		}
		// 50% enough traps to fill to soldiers-limit -> start using traps as they keeps building by themself
		else if(breeders < TrapperSend && TrapperSend <= breeders + trapable_trimps * 2)
		{
			PlayerGatheringSugestin = "trimps";
		}
		// 50% enough traps to fill to soldiers-limit -> start using traps as they keeps building by themself
		else if(game.resources.trimps.owned < TrapperSend && TrapperSend <= game.resources.trimps.owned + trapable_trimps * 2)
		{
			PlayerGatheringSugestin = "trimps";
		}
		// If you started using traps, keep using them till you used them all.
		if(game.global.playerGathering === "trimps" && game.buildings.Trap.owned > 10)
		{
			PlayerGatheringSugestin = "trimps";
		}
	}

	// Traps up to the first 100 Trimps
	if(game.triggers.Trap.done && game.resources.trimps.owned < 100 && max_trimps - game.resources.trimps.owned > TrapperSend)
	{
		if(game.global.buildingsQueue.length > game.upgrades.Trapstorm.done)
		{
			PlayerGatheringSugestin = "buildings";
		}
		else if(game.buildings.Trap.owned > 0)
		{
			PlayerGatheringSugestin = "trimps";
		}
		else if(game.resources.wood.owned < 20)
		{
			PlayerGatheringSugestin = "wood";
		}
		else if(game.resources.food.owned < 20)
		{
			PlayerGatheringSugestin = "food";
		}
		else
		{
			try_building("Trap", 2);
		}
	}

	// Science needed for upgrades
	if(!PlayerGatheringSugestin)
	{
		for(const upgrade_name of [
			"Battle",
			"Bloodlust",
			"Miners",
			"Speedminer",
			"Speedlumber",
			"Speedfarming",
			"Speedscience",
			"Speedexplorer",
			"Megaminer",
			"Megalumber",
			"Megafarming",
			"Megascience",
		])
		{

			const upgrade = game.upgrades[upgrade_name];
			if(!PlayerGatheringSugestin && upgrade.allowed > upgrade.done)
			{
				let science = upgrade.cost.resources.science;

				if(typeof science === "function")
				{
					science = science();
				}

				if(typeof science[1] !== "undefined")
				{
					science = Math.floor(science[0] * Math.pow(science[1], upgrade.done));
				}

				if(typeof science === "number" && science > game.resources.science.owned)
				{
					PlayerGatheringSugestin = "science";
					break;
				}
			}
		}
	}
	// Megascience can be bought
	if(!PlayerGatheringSugestin && game.upgrades.Megascience.allowed > game.upgrades.Megascience.done)
	{
		PlayerGatheringSugestin = "science";
	}
	// Speedscience can be bought
	if(!PlayerGatheringSugestin && game.upgrades.Speedscience.allowed > game.upgrades.Speedscience.done)
	{
		PlayerGatheringSugestin = "science";
	}
	// Buildqueue long
	if(!PlayerGatheringSugestin && game.global.buildingsQueue.length > 1)
	{
		PlayerGatheringSugestin = "buildings";
	}
	// Buildqueue, not autotraps
	if(!PlayerGatheringSugestin && game.global.buildingsQueue.length === 1 && game.global.buildingsQueue[0] !== "Trap.1")
	{
		PlayerGatheringSugestin = "buildings";
	}
	// Well fed
	if(!PlayerGatheringSugestin && game.global.turkimpTimer > 0)
	{
		PlayerGatheringSugestin = "metal";
	}
	// TODO science low Prio
	// Trapper challange, ideling, make more traps
	if(!PlayerGatheringSugestin && game.global.challengeActive === "Trapper")
	{
		PlayerGatheringSugestin = "buildings";
	}
	// Building nothing, gather metal instead
	if(!PlayerGatheringSugestin && game.global.buildingsQueue.length === 0 && game.global.playerGathering === "buildings")
	{
		PlayerGatheringSugestin = "metal";
	}
	// Openeing zero traps, gather metal instead
	if(!PlayerGatheringSugestin && game.buildings.Trap.owned === 0 && game.global.playerGathering === "trimps")
	{
		PlayerGatheringSugestin = "metal";
	}

	// Nothing selected yet?
	if(!PlayerGatheringSugestin)
	{
		PlayerGatheringSugestin = "metal";
	}

	// Metal avaible?
	if(PlayerGatheringSugestin === "metal")
	{
		if(!game.triggers.Battle.done || game.upgrades.Battle.done < 1)
		{
			PlayerGatheringSugestin = "science";
		}
	}

	// Science avaible?
	if(PlayerGatheringSugestin === "science" && !game.triggers.upgrades.done)
	{
		if(game.triggers.Trap.done < 1)
		{
			PlayerGatheringSugestin = "wood";
		}
		else if(game.resources.wood.owned < 20)
		{
			PlayerGatheringSugestin = "wood";
		}
		else
		{
			PlayerGatheringSugestin = "food";
		}
	}

	// Bloodlusy food
	if(PlayerGatheringSugestin === "science" && game.upgrades.Bloodlust.allowed > game.upgrades.Bloodlust.done && game.resources.science.owned >= 60 && game.resources.food.owned < 150)
	{
		PlayerGatheringSugestin = "food";
	}

	// Wood avaible?
	if(PlayerGatheringSugestin === "wood" && !game.triggers.wood.done)
	{
		PlayerGatheringSugestin = "food";
	}

	// Change gathering?
	if(PlayerGatheringSugestin && PlayerGatheringSugestin !== game.global.playerGathering)
	{
		switch(game.global.playerGathering)
		{
			case "metal":
			case "food":
			case "wood":
				if(PlayerGatheringSugestin !== "metal" || !game.triggers.Battle.done)
				{
					setGather(PlayerGatheringSugestin);
				}
				break;

			case "science":
			case "trimps":
			case "buildings":
			default:
				setGather(PlayerGatheringSugestin);
				break;
		}
	}
	//</editor-fold>

	//<editor-fold desc="AutoFight">
	const info_row = config.info_rows.map_status;

	if(config.auto_fight)
	{
		let map_price = (9 + 9 + 9) * ((game.global.world >= 60) ? 0.74 : 1) + 14 + game.global.world;
		map_price = 2 * Math.floor((((map_price / 150) * (Math.pow(1.14, map_price - 1))) * game.global.world * 2) * Math.pow((1.03 + (game.global.world / 50000)), game.global.world));

		const setMapRepeat = (locked, exitMap, exitNow) => {
			if(locked === true && game.options.menu.repeatUntil.enabled > 0)
			{
				game.options.menu.repeatUntil.enabled = 0;
				toggleSetting("repeatUntil", false, false, true);
			}
			else if(locked === false && game.options.menu.repeatUntil.enabled !== 3)
			{
				game.options.menu.repeatUntil.enabled = 3;
				toggleSetting("repeatUntil", false, false, true);
			}
			if(exitMap === true && game.options.menu.exitTo.enabled || exitMap === false && !game.options.menu.exitTo.enabled)
			{
				toggleSetting("exitTo");
			}
			if(exitNow === true && game.global.repeatMap || exitNow === false && !game.global.repeatMap)
			{
				repeatClicked();
			}
		};
		const setMapLevel = (loot, difficulty, size) => {
			document.getElementById("lootAdvMapsRange").value = loot;
			adjustMap("loot", loot);
			document.getElementById("difficultyAdvMapsRange").value = difficulty;
			adjustMap("difficulty", difficulty);
			document.getElementById("sizeAdvMapsRange").value = size;
			adjustMap("size", size);
		};
		const useOrBuyMap = (cheapFallback, wordlFallback) => {
			if(game.global.mapsActive)
			{
				if(game.global.pauseFight)
				{
					pauseFight();
				}
				if(
					upgrades_to_buy < 1 &&
					upgrades_to_pickup > 0 &&
					game.global.mapBonus >= 9 &&
					addSpecials(true, true, getCurrentMapObject()) < 1
				)
				{
					setMapRepeat(false, true, true);
				}
				return;
			}

			if(game.global.fighting)
			{
				if(!game.global.pauseFight)
				{
					pauseFight();
				}
				return;
			}

			if(game.global.mapsOwnedArray.length > 99)
			{
				document.getElementById("mapLevelInput").value = game.global.world - 5;
				recycleBelow(true);
			}

			let best_map = null;
			let best_map_level = -1;
			for(const map in game.global.mapsOwnedArray)
			{
				if(!game.global.mapsOwnedArray.hasOwnProperty(map))
				{
					continue;
				}
				if(game.global.mapsOwnedArray[map].noRecycle)
				{
					continue;
				}
				if(game.global.mapsOwnedArray[map].level <= best_map_level)
				{
					continue;
				}
				best_map_level = game.global.mapsOwnedArray[map].level;
				best_map = map;
			}
			if(best_map_level + game.portal.Siphonology.level < game.global.world)
			{
				best_map_level = -1;
			}

			// Prio expensive map
			if(
				best_map_level < game.global.world &&
				upgrades_to_buy < 1 &&
				upgrades_to_pickup > 0 &&
				game.global.mapBonus >= 9 &&
				map_price > game.resources.fragments.owned &&
				(!cheapFallback || game.global.world === 200 || game.global.world === config.void_map_level)
			)
			{
				best_map_level = -1;
			}
			if(best_map_level > 0)
			{
				// use map
				if(!game.global.preMapsActive && !game.global.switchToMaps)
				{
					mapsClicked();
				}

				if(!game.global.preMapsActive)
				{
					return;
				}

				if(game.global.currentMapId !== "")
				{
					return;
				}

				selectMap(game.global.mapsOwnedArray[best_map].id);

				if(game.global.lookingAtMap !== game.global.mapsOwnedArray[best_map].id)
				{
					return;
				}

				runMap();
				setMapRepeat(true, false, false);
				return;
			}

			if(map_price > game.resources.fragments.owned)
			{
				map_price = 2 * Math.floor((((game.global.world / 150) * (Math.pow(1.14, game.global.world - 1))) * game.global.world * 2) * Math.pow(
					(1.03 + (game.global.world / 50000)),
					game.global.world,
				));
				if(!cheapFallback || map_price > game.resources.fragments.owned)
				{
					if(wordlFallback)
					{
						if(game.global.preMapsActive)
						{
							mapsClicked();
						}
						fightManual();
					}
					if(best_map !== null)
					{
						selectMap(game.global.mapsOwnedArray[best_map].id);

						if(game.global.lookingAtMap !== game.global.mapsOwnedArray[best_map].id)
						{
							return;
						}

						runMap();
					}
					return;
				}

				// Buy cheep map
				if(!game.global.preMapsActive && !game.global.switchToMaps)
				{
					mapsClicked();
				}
				if(game.global.preMapsActive)
				{
					setMapLevel(0, 0, 0);
					document.getElementById("biomeAdvMapsSelect").value = "Random";
					document.getElementById("advSpecialSelect").value = 0;
					document.getElementById("mapLevelInput").value = game.global.world;
					buyMap();
					setMapRepeat(true, false, false);
				}
				return;
			}

			// Buy expensive map
			if(!game.global.preMapsActive && !game.global.switchToMaps)
			{
				mapsClicked();
			}
			if(game.global.preMapsActive)
			{
				setMapLevel(9, 9, 9);
				document.getElementById("biomeAdvMapsSelect").value = "Plentiful";
				document.getElementById("advSpecialSelect").value = "hc";
				document.getElementById("mapLevelInput").value = game.global.world;
				buyMap();
				setMapRepeat(true, false, false);
			}
		};
		const mapOrWorld = (farm) => {
			if(game.global.mapsActive)
			{
				setMapRepeat(farm, null, null);
			}
			if(game.global.pauseFight)
			{
				pauseFight();
			}
			if(game.global.preMapsActive)
			{
				mapsClicked();
			}
		};
		const doVoidMaps = () => {

			// Farm first
			if((upgrades_to_buy > 0 || upgrades_to_pickup > 0 || game.global.mapBonus >= 9) && map_price > game.resources.fragments.owned)
			{
				useOrBuyMap(true, false);
			}

			// In a map?
			if(game.global.mapsActive)
			{
				// Turn of auto-fight
				if(game.global.pauseFight)
				{
					pauseFight();
				}

				// Exit when done
				setMapRepeat(false, true, true);
				return;
			}
			// In world-map?
			if(!game.global.preMapsActive)
			{
				// Activate switch to map
				if(!game.global.switchToMaps)
				{
					mapsClicked();
				}
				// Turn of auto-fight
				if(game.global.fighting)
				{
					if(!game.global.pauseFight)
					{
						pauseFight();
					}
				}
				return;
			}

			// In normal map-selection
			if(!game.global.voidMapsToggled) {
				toggleVoidMaps(false);
				return;
			}

			// In void-map-selection
			var mapIndex;
			var mapCount = game.global.mapsOwnedArray.length;
			for(mapIndex = 0; mapIndex < mapCount; mapIndex++)
			{
				if(game.global.mapsOwnedArray[mapIndex].location === "Void")
				{
					selectMap(game.global.mapsOwnedArray[mapIndex].id);
					if(game.global.lookingAtMap !== game.global.mapsOwnedArray[mapIndex].id)
					{
						return;
					}
					runMap();
					setMapRepeat(false, true, false);
					return;
				}
			}
		};
		const doPortal = () => {
			// Turn on auto-fight, don't stop if we in the flow...
			if(game.global.pauseFight)
			{
				pauseFight();
			}
			if(game.global.fighting)
			{
				if(game.global.mapsActive)
				{
					setMapRepeat(false, true, false);
				}
				return;
			}

			if(game.global.heirloomsExtra.length)
			{
				recycleAllExtraHeirlooms();
				return;
			}
			if(!portalWindowOpen)
			{
				portalClicked();
				return;
			}
			if(game.global.selectedChallenge === "Corrupted") {
				selectChallenge("Corrupted");
				return;
			}

			if(!game.global.lockTooltip)
			{
				activateClicked();
			}
			else
			{
				activatePortal();
			}

			// Why do heliumLeftover get out of sync
			// game.global.heliumLeftover = game.global.totalHeliumEarned - game.stats.spentOnWorms.valueTotal - countHeliumSpent() - game.resources.helium.owned;
		};

		if(game.global.roboTrimpLevel && !game.global.useShriek && (game.global.world % 5 === 0) && game.global.roboTrimpCooldown <= 0)
		{
			magnetoShriek();
		}

		let ratio = 0;
		const enemy_attack = game.global.gridArray[game.global.lastClearedCell + 1].attack;
		if(combat_values.attack > 0 && combat_values.ref_enemy > 0)
		{
			ratio = combat_values.ref_enemy / combat_values.attack;
		}

		// Autofight pre Bloodlust
		if(game.upgrades.Bloodlust.allowed < 1 && game.upgrades.Battle.done > 0 && !game.global.fighting && config.auto_fight)
		{
			puggan_auto_trimps.info(info_row, "M: Earn Battle");
			mapOrWorld(false);
			// Wait to fight until the breeding is done, but if we buy houses to fast, start the fight when we have at least 10 000 trims
			if(max_trimps <= game.resources.trimps.owned || game.resources.trimps.owned > game.resources.trimps.maxSoldiers * 1e4)
			{
				fightManual();
			}
		}
		else if(game.global.world === 200 && game.global.spireActive && config.spire_wait !== null)
		{
			// TODO game.global.spireDeaths, 1M% -> first try, 2M% -> 2nd try, 15M% -> 3-5:th try, Maxed -> 6-8:th try
			if(combat_values.attack > 15e4 * combat_values.ref_enemy && game.global.spireDeaths < 5 ||
				combat_values.attack > 2e4 * combat_values.ref_enemy && game.global.spireDeaths < 2 ||
				combat_values.attack > 1e4 * combat_values.ref_enemy && game.global.spireDeaths < 1)
			{
				puggan_auto_trimps.info(info_row, "M: Spire Power " + (game.global.spireDeaths  + 1));

				// If not in world / spire, go to world
				if(game.global.mapsActive || game.global.preMapsActive)
				{
					mapOrWorld(false);
				}
				// If fighting in Spire, turn off auto
				else if(game.global.fighting && !game.global.pauseFight)
				{
					pauseFight();
				}
				// If not fighting in Spire, turn on auto
				else if(!game.global.fighting && game.global.pauseFight)
				{
					pauseFight();
				}
			}
			else if(config.spire_wait === false)
			{
				puggan_auto_trimps.info(info_row, "M: Spire Exit");
				// exit spire
				endSpire();
				mapOrWorld(true);
			}
			else if(game.upgrades.Gigastation.done < game.upgrades.Gigastation.allowed && game.upgrades.Coordination.done < game.upgrades.Coordination.allowed)
			{
				puggan_auto_trimps.info(info_row, "M: Spire Giga");
				// Keep farming, and buy more gigastations
				useOrBuyMap(true, false);
			}
			else if(upgrades_to_pickup > 0 || upgrades_to_buy > 0 || game.equipment.Arbalest.level < config.max_equipment || game.equipment.Gambeson.level < config.max_equipment)
			{
				puggan_auto_trimps.info(info_row, "M: Spire Equipment");
				// Keep farming, and buy more upgrades
				useOrBuyMap(true, false);
			}
			else
			{
				puggan_auto_trimps.info(info_row, "M: Spire Run");
				// Lets try the spire
				mapOrWorld(false);
			}
		}
		else if(game.global.totalVoidMaps > 0 && game.global.world === config.void_map_level)
		{
			puggan_auto_trimps.info(info_row, "M: Void Map");
			doVoidMaps();
		}
		else if(config.auto_portal > 0 && game.global.world >= config.auto_portal)
		{
			puggan_auto_trimps.info(info_row, "M: Portal");
			doPortal();
		}
		else if(upgrades_to_pickup < 1 && upgrades_to_buy < 1 && game.global.mapBonus >= 10)
		{
			puggan_auto_trimps.info(info_row, "M: World");
			// nothing left to pick up or buy
			mapOrWorld(false);
		}
		else if(game.global.mapBonus < 10 && ratio > config.auto_fight_bonus)
		{
			puggan_auto_trimps.info(info_row, "M: Map Bonus");
			useOrBuyMap(true, false);
		}
		else if(ratio > config.auto_fight_farm)
		{
			if(upgrades_to_pickup > 0)
			{
				puggan_auto_trimps.info(info_row, "M: P/Dmg");
			}
			else
			{
				puggan_auto_trimps.info(info_row, "M: Damage");
			}
			useOrBuyMap(false, true);
		}
		else if(upgrades_to_pickup > 10)
		{
			puggan_auto_trimps.info(info_row, "M: Prestiges");
			useOrBuyMap(true, true);
		}
		else if(combat_values.hp/2 < enemy_attack && (combat_values.hp/2 < enemy_attack * combat_values.pierce || combat_values.block/2 + combat_values.hp/2 < enemy_attack))
		{
			puggan_auto_trimps.info(info_row, "M: Health");
			useOrBuyMap(false, true);
		}
		else if(!game.global.mapsActive)
		{
			mapOrWorld(null);
			puggan_auto_trimps.info(info_row, "M: Ok");
		}
		else if(ratio > config.auto_fight_farm / 2)
		{
			puggan_auto_trimps.info(info_row, "M: Stay");
			mapOrWorld(null);
		}
		else
		{
			puggan_auto_trimps.info(info_row, "M: Finnish");
			mapOrWorld(false);
		}
	}
	else
	{
		puggan_auto_trimps.info(info_row, "M: Off");
	}
	//</editor-fold>

	if(game.global.world >= 230 && game.options.menu.generatorStart.enabled === 1 && game.generatorUpgrades.Overclocker.upgrades < 0)
	{
		const maxFuel = getGeneratorFuelCap();
		const prefMode = (game.global.magmaFuel + 0.2 < maxFuel) ? 1 : 0;
		if(game.global.generatorMode !== prefMode)
		{
			changeGeneratorState(prefMode);
		}
	}

	game.global.buyAmt = amount;
};

if(puggan_auto_trimps.timers.play)
{
	clearInterval(puggan_auto_trimps.timers.play);
}
puggan_auto_trimps.timers.play = setInterval(puggan, 1000);
puggan();
