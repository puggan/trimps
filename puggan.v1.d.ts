/// <reference path="./game.d.ts" />

import {trimps} from "./game";

declare function storage_max(resource_name: trimps.resource_name): number;
declare function storage_total_time(resource_name: trimps.resource_name): number;
declare function storage_time_to_max(resource_name: trimps.resource_name): number;
declare function puggan(): void;

declare interface EquipmentDescriptions {
	equipment_name: trimps.equipment_name;
	equipment_type: "Armor" | "Weapon";
	upgrade_name: trimps.upgrade_name;
	upgrade_offset: number;
}
