window.prettify = number => {
	if(!isFinite(number))
	{
		return "∞";
	}
	if(number >= 1000 && number < 10000)
	{
		return Math.floor(number);
	}
	if(number === 0)
	{
		return prettifySub(0);
	}
	if(number < 0)
	{
		return "-" + prettify(-number);
	}
	if(number < 0.005)
	{
		return "0.00";
	}

	let base = Math.floor(Math.log(number) / Math.log(1000));
	if(base === Infinity) {
		return "∞";
	}
	if(base <= 0)
	{
		return prettifySub(number);
	}
	number /= Math.pow(1000, base);
	if(number >= 999.5)
	{
		// 999.5 rounds to 1000 and we don’t want to show “1000K” or such
		number /= 1000;
		++base;
	}
	const suffices = ["K", "M", "G", "T", "P", "E", "Z", "Y"];
	if(base > suffices.length)
	{
		return prettifySub(number) + "e" + ((base) * 3);
	}
	return prettifySub(number) + suffices[base - 1];
};
