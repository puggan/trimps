function getRemainingEssenceDrops()
{
	const cells = [];
	// game.global.lastClearedCell is 2 lower then current cell shown, at last cell game.global.lastClearedCell is 98
	const cellsRemaining = 100 - game.global.lastClearedCell - 1;
	for(let x = 1; x <= cellsRemaining; x++)
	{
		const roll = getRandomIntSeeded(game.global.scrySeed + x, 0, 100);
		if(roll < 50 || roll > 52)
		{
			continue;
		}
		cells.push(game.global.lastClearedCell + x + 1);
	}
	return cells;
}

function paintRemainingEssenceDrops()
{
	const cells = getRemainingEssenceDrops();
	if(cells.length < 1)
	{
		return;
	}
	for(const cell_nr of cells)
	{
		const cell_id = "cell" + (cell_nr - 1);
		const cell = document.getElementById(cell_id);
		if(cell.getElementsByClassName("essence").length > 0)
		{
			continue;
		}
		const span = document.createElement("span");
		span.classList.add("essence");
		span.innerText = "S";
		cell.appendChild(span);
	}
}
