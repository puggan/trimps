/* tslint:disable:no-console */
/// <reference path="./game.d.ts" />

if(!window.auto_timer)
{
	auto_timer = 0;
}
if(!window.puggan)
{
	puggan = null;
}

puggan = () => {
	const amount = game.global.buyAmt;
	const job_fire_mode = game.global.firing;
	game.global.firing = false;
	game.global.buyAmt = 1;

	// TODO
	/*
	 * Prio:
	 * 0. If storage is full, prio them
	 * 1. If unemployed trimps, employ them
	 * 2. If normal upgrades, prio them
	 * 3. If gymnasitc upgrede, prio it
	 * 4. If Cordintaion require more trimps, prio houses
	 * 4a. Highest house vs other houses useing same mats
	 * 5. If prestige upgrades is avaible prio them
	 * 6. If health is bellow void map requirements, prio healt equipment
	 * 6a. Wood: Shield vs Gym
	 * 6b. Food: Trainers
	 * 6c. Metal: Health equipment
	 * 7. If Attack equipment are falling behind Health equipment, upgrade them too
	 *
	 */

	game.global.firing = job_fire_mode;
	game.global.buyAmt = amount;
};
puggan();

if(auto_timer)
{
	clearInterval(auto_timer);
}
auto_timer = setInterval(puggan, 3000);
