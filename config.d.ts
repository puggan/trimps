/* tslint:disable:no-namespace */

declare namespace puggan_auto_trimps_config
{
	interface Timers
	{
		config: number;
		formation: number;
		play: number;
		save: number;
	}
	interface Config
	{
		auto_employ: boolean;
		auto_fight: boolean;
		auto_fight_bonus: number;
		auto_fight_farm: number;
		auto_portal: false|number;
		geneticist_min_time: number;
		gigastation_delta: number;
		gigastation_init: number;
		info_rows: InfoRows;
		portal?: number;
		max_build_queue: number;
		max_equipment: number;
		max_equipment_shield: number;
		max_explorer: number;
		max_houses: number;
		max_map_equipment: number;
		max_nursery: number;
		nursery_max_time: number;
		potency_max_time: number;
		prefered_stance: "X"|"H"|"D"|"B"|"S";
		prio_gold_battle: boolean;
		prio_gym_until: number;
		prio_shield_until: number;
		spire_wait: null|boolean;
		storage_min_count: number;
		storage_min_time: number;
		void_map_level: number;
	}
	interface Global
	{
		config: Config;
		next_portal_config: Config;
		info: ((p: number, s: string, n?: number) => void)|(() => void);
		timers: Timers;
	}
	interface CombatValues
	{
		attack: number;
		block: number;
		hp: number;
		pierce: number;
		ref_enemy: number;
	}
	interface InfoRows
	{
		attack: number;
		attack_speed: number;
		enemy: number;
		equipment: number;
		health: number;
		map_status: number;
	}
}
declare interface Window
{
	combat_values: puggan_auto_trimps_config.CombatValues;
	puggan_auto_trimps: puggan_auto_trimps_config.Global;
}
declare var portalWindowOpen: boolean;
declare var combat_values: puggan_auto_trimps_config.CombatValues;
declare var puggan_auto_trimps: puggan_auto_trimps_config.Global;
