(() => {
	let pinfo = document.getElementById("puggan_infobox");
	if(!pinfo)
	{
		pinfo = document.createElement("div");
		pinfo.id = "puggan_infobox";
		pinfo.setAttribute("style", "font-size: 1.1vw; text-align: center; background-color: rgba(0, 0, 0, 0.3); padding: 5%");
		const column = document.getElementById("battleBtnsColumn");
		column.appendChild(pinfo);
		let pbut = document.createElement("span");
		pbut.id = "puggan_fight_button";
		pbut.classList.add("btn");
		pbut.classList.add("btn-warning");
		pbut.setAttribute("style", "width: 100%; font-size: 1.1vw; padding: 0.15vw 0.3vw;");
		pbut.innerText = "P-Fight";
		let pbutc = document.createElement("div");
		pbutc.classList.add("battleSideBtnContainer");
		pbutc.appendChild(pbut);
		column.appendChild(pbutc);

		const fight_update = (updateOnly) => {
			pbut.classList.remove("btn-warning");
			pbut.classList.remove("btn-success");
			pbut.classList.remove("btn-danger");
			if(updateOnly !== true)
			{
				puggan_auto_trimps.config.auto_fight = !puggan_auto_trimps.config.auto_fight;
			}
			if(puggan_auto_trimps.config.auto_fight) {
				pbut.classList.add("btn-success");
			} else {
				pbut.classList.add("btn-danger");
			}
		};
		pbut.addEventListener("click", fight_update);
		fight_update(true);
	}
	pinfo.setAttribute("style", "font-size: 1.1vw; text-align: center; background-color: rgba(0, 0, 0, 0.3); padding: 5%");
	pinfo.innerHTML = "<div>Loading</div>";

	/**
	 * Set row in info box
	 *  1: A / E in % or E / A in s
	 *  2: A: Attackpower
	 *  3: E: Reference Enemy health, a Grimp enemy at level 50 on next world map
	 *  4: Prestig items left to pick up / Prestige itmes not bought
	 * @param {number} p row number
	 * @param {string} s row content
	 * @param {number} n row value
	 */
	window.puggan_auto_trimps.info = (p, s, n) => {
		if(p < 1)
		{
			return;
		}
		while(pinfo.childElementCount < p) {
			pinfo.appendChild(document.createElement("div"));
		}
		if(n)
		{
			pinfo.children[p - 1].innerText = s + prettify(n);
		}
		else
		{
			pinfo.children[p - 1].innerText = s;
		}
	};
})();
