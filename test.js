/*
const total = 150;
const f = 60;
const l = 10;
const m = 10;
const freeWorkers = total - f - l - m;
const game = {resources: {trimps: {employed: f + l + m}}, jobs: {Farmer: false, Lumberjack: false, Miner: false}};

const employ = (job, count) => console.log('Employing ' + count + ' at ' + job);
const calculateMaxAfford = (a,b,c,d) => 1e6;

if(freeWorkers > 0)
{

	const jobs = {
		f: {
			n: 'Farmer',
			c: f,
			e: f,
			a: 0,
		},
		l: {
			n: 'Lumberjack',
			c: l,
			e: l,
			a: 0,
		},
		m: {
			n: 'Miner',
			c: m,
			e: m,
			a: 0,
		},
	};

	let job_list = [jobs.f, jobs.l, jobs.m];

	if(m > l) {
		if(l > f) {
			job_list = [jobs.m, jobs.l, jobs.f];
		}
		else if(m > f) {
			job_list = [jobs.m, jobs.f, jobs.l];
		}
		else {
			job_list = [jobs.f, jobs.m, jobs.l];
		}
	} else if(m > f) {
		job_list = [jobs.l, jobs.m, jobs.f];
	} else if(l > f) {
		job_list = [jobs.l, jobs.f, jobs.m];
	}

	let goal = game.resources.trimps.employed + freeWorkers;
	let expected = Math.floor(goal / 3);

	if(job_list[0].c < expected) {
		job_list[0].e = expected;
		job_list[0].a = expected - job_list[0].c;
		goal -= expected;
	} else {
		goal -= job_list[0].c;
	}

	expected = Math.floor(goal / 2);
	if(job_list[1].c < expected) {
		job_list[1].e = expected;
		job_list[1].a = expected - job_list[1].c;
		goal -= expected;
	} else {
		goal -= job_list[1].c;
	}

	if(job_list[2].c < goal) {
		job_list[2].e = goal;
		job_list[2].a = goal - job_list[2].c;
	}

	console.log(job_list);
	if(job_list[2].a) {
		let afford_jobs = calculateMaxAfford(game.jobs[job_list[2].n], false, false, true);
		if(afford_jobs > job_list[2].a) {
			employ(job_list[2].n, job_list[2].a);
		} else if(afford_jobs > 0) {
			employ(job_list[2].n, afford_jobs);
		}
	}
}
*/
