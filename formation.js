/* tslint:disable:no-console */
/// <reference path="./game.d.ts" />
/// <reference path="./config.d.ts" />

if(!window.combat_values)
{
	combat_values = {
		attack: 0,
		block: 0,
		hp: 0,
		pierce: 0,
		ref_enemy: 0,
	};
}

puggan_formation = () => {
	const get_combat_values = () => {
		if(game.global.soldierHealth > 0)
		{
			/** @type {puggan_auto_trimps_config.CombatValues} current_values */
			let current_values = {
				attack: game.global.soldierCurrentAttack,
				block: game.global.soldierCurrentBlock,
				hp: game.global.soldierHealthMax,
				pierce: game.global.world < 60 ? 0 : getPierceAmt(),
				ref_enemy: 0,
				ref_enemy_max: 0,
				ref_enemy_min: 0,
			};

			current_values.attack *= game.jobs.Amalgamator.getDamageMult();
			current_values.attack *= (1 + (game.global.achievementBonus / 100));
			current_values.attack *= ((game.global.antiStacks * game.portal.Anticipation.level * game.portal.Anticipation.modifier) + 1);
			current_values.attack *= (1 + (game.global.roboTrimpLevel * 0.2));
			current_values.attack *= (1 + game.goldenUpgrades.Battle.currentBonus);

			if (game.talents.voidPower.purchased && game.global.voidBuff){
				current_values.attack *= ((((game.talents.voidPower2.purchased) ? ((game.talents.voidPower3.purchased) ? 65 : 35) : 15) / 100) + 1);
			}

			current_values.attack *= (1 + (game.global.totalSquaredReward / 100));
			current_values.attack *= game.empowerments.Ice.getCombatModifier();
			current_values.attack *= game.jobs.Magmamancer.getBonusPercent();

			if(game.talents.stillRowing2.purchased)
			{
				current_values.attack *= 1 + (0.06 * game.global.spireRows);
			}

			if (game.talents.healthStrength.purchased && mutations.Healthy.active()){
				current_values.attack *= 1 + (0.15 * mutations.Healthy.cellCount());
			}

			if (game.singleRunBonuses.sharpTrimps.owned){
				current_values.attack *= 1.5;
			}

			current_values.attack = calcHeirloomBonus("Shield", "trimpAttack", current_values.attack);
			if (Fluffy.isActive()){
				current_values.attack *= Fluffy.getDamageModifier();
			}

			switch(game.global.formation)
			{
				// Heap 4x Health
				case 1:
					current_values.attack *= 2;
					current_values.hp /= 4;
					current_values.block *= 2;
					break;

				// Dominance 4x Damage
				case 2:
					current_values.attack /= 4;
					current_values.hp *= 2;
					current_values.block *= 2;
					break;

				// Barrier 4x Block
				case 3:
					current_values.attack *= 2;
					current_values.hp *= 2;
					current_values.block /= 4;
					current_values.pierce *=2;
					break;

				// Scryer 2x Loot
				case 4:
					current_values.attack *= 2;
					current_values.hp *= 2;
					current_values.block *= 2;
					break;

				case 0:
				default:
					break;
			}
			window.combat_values = current_values;
		}

		let enemyHealth = 130 * Math.sqrt(game.global.world + 1) * Math.pow(3.265, 0.5 + game.global.world / 2) - 110;
		enemyHealth = game.global.world < 59 ?
			((enemyHealth * 0.4) + ((enemyHealth * 0.4) * (50 / 110))) * 0.75 :
			((enemyHealth * 0.5) + ((enemyHealth * 0.8) * (50 / 100))) * Math.pow(
			1.1,
			game.global.world - 58,
			);
		// TODO
		let enemyAttack = 0;

		window.combat_values.ref_enemy = Math.floor(enemyHealth * game.badGuys.Grimp.health);

		return window.combat_values;
	};

	const combat_values = get_combat_values();

	let as = prettify(combat_values.ref_enemy / combat_values.attack / 2) + "s";
	if(combat_values.attack >= combat_values.ref_enemy)
	{
		as = prettify(100 * combat_values.attack / combat_values.ref_enemy) + "%";
	}
	as = "W " + (game.global.world + 1) + " @ " + as;
	const config = puggan_auto_trimps.config;
	puggan_auto_trimps.info(config.info_rows.attack_speed, as);
	puggan_auto_trimps.info(config.info_rows.attack, "A: ", combat_values.attack);
	puggan_auto_trimps.info(config.info_rows.enemy, "E: ", combat_values.ref_enemy);

	const config_prefered = config.prefered_stance;

	// 0: plain
	// 1: game.upgrades.Formations.done
	// 2: game.upgrades.Dominance.done
	// 3: game.upgrades.Barrier.done
	// 4: Scryer highestLevelCleared >= 180
	if(game.upgrades.Formations.done && game.global.gridArray.length > 0)
	{
		const mapEnemy = game.global.mapGridArray[game.global.lastClearedMapCell + 1];
		const worldEmeny = game.global.gridArray[game.global.lastClearedCell + 1];
		const enemy = game.global.mapsActive ? mapEnemy : worldEmeny;

		if(enemy)
		{
			let fast = game.global.voidBuff === "doubleAttack" || game.global.challengeActive === "Slow" || game.badGuys[enemy.name].fast;
			let prefered_formation = 0;

			if(game.global.world >= 60 && game.global.highestLevelCleared >= 180)
			{
				prefered_formation = 4;
			}
			else if(game.upgrades.Dominance.done)
			{
				prefered_formation = 2;
			}

			const enemy_damage = enemy.attack;
			const enemy_min_dmg = Math.floor(enemy.attack * 0.8);
			const enemy_max_dmg = Math.floor(enemy.attack * 1.2);

			const baseBH = Math.min(combat_values.hp / combat_values.pierce, combat_values.block + combat_values.hp);
			const halfBH = baseBH / 2;
			const healthBH = Math.min(combat_values.hp / combat_values.pierce, combat_values.block / 8 + combat_values.hp) * 4;
			const blockBH = Math.min(combat_values.hp / combat_values.pierce, combat_values.block * 4 + combat_values.hp / 2);

			if(worldEmeny.attack > 0) {

				//const enemy_min_pierce_dmg = Math.max(combat_values.pierce * worldEmeny.attack * 0.8, worldEmeny.attack * 0.8 - combat_values.block);
				const enemy_max_pierce_dmg = Math.max(combat_values.pierce * worldEmeny.attack * 1.2, worldEmeny.attack * 1.2 - combat_values.block/2);
				if(healthBH < worldEmeny.attack && blockBH < worldEmeny.attack)
				{
					puggan_auto_trimps.info(config.info_rows.health, "H: 1 hit Stall Any");
				}
				else if(halfBH < worldEmeny.attack)
				{
					puggan_auto_trimps.info(config.info_rows.health, "H: 1 hit Stall D/S");
				}
				else if(enemy_max_pierce_dmg === 0)
				{
					puggan_auto_trimps.info(config.info_rows.health, "H: Block all");
				}
				else
				{
					const survive_rate = Math.floor(50*combat_values.hp/enemy_max_pierce_dmg);
					if(survive_rate > 50000)
					{
						puggan_auto_trimps.info(config.info_rows.health, "H: " + prettify(survive_rate / 100) + "x");
					}
					else if(survive_rate > 500)
					{
						puggan_auto_trimps.info(config.info_rows.health, "H: " + survive_rate / 100 + "x");
					}
					else if(survive_rate < 0)
					{
						puggan_auto_trimps.info(config.info_rows.health, "H: " + prettify(survive_rate / 100) + "x");
						console.error({
							survive_rate: survive_rate,
							enemy_max_pierce_dmg: enemy_max_pierce_dmg,
							enemy_max_pierce_dmg_p: combat_values.pierce * worldEmeny.attack * 1.2,
							enemy_max_pierce_dmg_b: worldEmeny.attack * 1.2 - combat_values.block/2,
							worldEmeny_attack: worldEmeny.attack,
							combat_values: {...combat_values},
						});
					}
					else
					{
						puggan_auto_trimps.info(config.info_rows.health, "H: " + survive_rate + "%");
					}
				}
			}

			if(fast)
			{
				// If we in all formation, survives at least 50% of the time, don't care about enemy being fast.
				if(enemy_damage >= halfBH)
				{
					// If we can get Dark Essence, use Scryer, even if the survival chance is tiny.
					if(game.global.world >= 181 && enemy_min_dmg < halfBH && config_prefered === "S")
					{
						prefered_formation = 4;
					}
					// Do we have 100% chance to survive first hit in normal formation?
					else if(enemy_max_dmg < baseBH)
					{
						prefered_formation = 0;
					}
					// Can we survive 50% of the first attack in block formation?
					else if(game.upgrades.Barrier.done && enemy_damage < blockBH)
					{
						prefered_formation = 3;
					}
					// Do we have any chance to survive in Health Formation
					else if(enemy_min_dmg < healthBH)
					{
						prefered_formation = 1;
					}
					// Impossible to survive
					else
					{
						prefered_formation = 0;
					}
				}
			}

			switch(config_prefered)
			{
				case "D":
					if(prefered_formation === 4)
					{
						prefered_formation = 2;
					}
					break;

				case "X":
					if(prefered_formation === 4 || prefered_formation === 2)
					{
						prefered_formation = 0;
					}
					break;

				case "B":
					prefered_formation = 3;
					break;

				case "H":
					prefered_formation = 1;
					break;

				case "S":
				default:
					break;
			}

			// Spire
			if(game.global.world === 200 && game.global.spireActive && !game.global.mapsActive)
			{
				prefered_formation = 2;
			}

			if(config.auto_fight && game.global.formation !== prefered_formation)
			{
				setFormation(prefered_formation ? prefered_formation : "0");
				if(game.global.soldierHealth > 0)
				{
					game.global.current_formation = prefered_formation;
				}
			}
		}
	}

	if(game.global.challengeActive === "Trapper" && !game.global.fighting)
	{
		const missing_trimps = game.resources.trimps.getCurrentSend() - game.resources.trimps.owned;
		if(missing_trimps > 0) {
			const trimps_per_trap = 1+game.portal.Bait.level;
			const required_traps = Math.ceil(missing_trimps / trimps_per_trap);
			const missing_traps = Math.max(0, required_traps/2 - game.buildings.Trap.owned);
			const waittime_in_ticks = required_traps + missing_traps;
			puggan_auto_trimps.info(config.info_rows.health, "T: " + (new Date(Date.now() + waittime_in_ticks*100)).toTimeString().substr(0,8));
		}
	}
};

if(puggan_auto_trimps.timers.formation)
{
	clearInterval(puggan_auto_trimps.timers.formation);
}

puggan_auto_trimps.timers.formation = setInterval(puggan_formation, 480);
