/// <reference path="./game.d.ts" />

voidmap = (va) => {
	const vd = [
		0,
		0,
		0,
		0,
		0,
		71e2, //5
		141e2,
		303e2,
		586e2,
		112e3,
		214e3,
		407e3,
		768e3,
		145e4,
		271e4,
		508e4,
		949e4,
		177e5,
		329e5,
		611e5,
		113e6,
		21e7,
		389e6,
		719e6,
		133e7,
		245e7,
		452e7,
		833e7,
		153e8,
		282e8,
		519e8,
		955e8,
		175e9,
		322e9,
		591e9,
		108e10,
		199e10,
		365e10,
		668e10,
		122e11,
		224e11,
		41e12,
		751e11,
		137e12,
		251e12,
		46e13,
		841e12,
		154e13,
		281e13,
		513e13,
		9.37e15,
		1.71e16,
		3.13e16,
		5.71e16,
		1.04e17,
		1.9e17,
		3.47e17,
		6.33e17,
		1.15e18,
		2.1e18,
		1.12e19,
		2.35e19,
		4.94e19,
		1.03e20,
		2.17e20,
		4.54e20,
		9.52e20,
		2e21,
		4.18e21,
		8.76e21,
		1.83e22,
		3.84e22,
		8.04e22,
		1.68e23,
		3.53e23,
		7.38e23,
		1.55e24,
		3.24e24,
		6.77e24,
		1.42e25,
		2.97e25,
		6.21e25,
		1.3e26,
		2.72e26,
		5.68e26,
		1.19e27,
		2.49e27,
		5.2e27,
		1.09e28,
		2.28e28,
		4.76e28,
		9.95e28,
		2.08e29,
		4.35e29,
		9.09e29,
		1.9e30,
		3.97e30,
		8.31e30,
		1.74e31,
		3.63e31,
		7.59e31,
		1.59e32,
		3.31e32,
		6.92e32,
		1.45e33,
		3.02e33,
		6.32e33,
		1.32e34,
		2.76e34,
		5.76e34,
		1.2e35,
		2.51e35,
		5.25e35,
		1.1e36,
		2.29e36,
		4.79e36,
		1e37,
		2.09e37,
		4.36e37,
		9.1e37,
		1.9e38,
		3.97e38,
		8.29e38,
		1.73e39,
		3.61e39,
		7.55e39,
		1.58e40,
		3.29e40,
		6.87e40,
		1.43e41,
		2.99e41,
		6.25e41,
		1.3e42,
		2.72e42,
		5.68e42,
		1.19e43,
		2.48e43,
		5.17e43,
		1.08e44,
		2.25e44,
		4.7e44,
		9.81e44,
		2.05e45,
		4.27e45,
		8.91e45,
		1.86e46,
		3.88e46,
		8.1e46,
		1.69e47,
		3.53e47,
		7.36e47,
		1.53e48,
		3.2e48,
		6.68e48,
		1.39e49,
		2.91e49,
		6.07e49,
		1.27e50,
		2.64e50,
		5.51e50,
		1.15e51,
		2.4e51,
		5e51,
		1.04e52,
		2.18e52,
		4.54e52,
		9.47e52,
		1.97e53,
		4.12e53,
		8.59e53,
		1.79e54,
		3.74e54,
		7.79e54,
		1.63e55,
		3.39e55,
		7.07e55,
		1.47e56,
		3.07e56,
		6.41e56,
		1.34e57,
		2.79e57,
		1.11e58,
		2.32e58,
		4.84e58,
		1.01e59,
		2.1e59,
		4.61e59,
		9.61e59,
		2e60,
		4.18e60,
		8.71e60,
		1.82e61,
		3.98e61,
		8.29e61,
		1.73e62,
		3.6e62,
		7.51e62,
		1.57e63,
		3.43e63,
		7.15e63,
		1.49e64,
		3.11e64,
		6.48e64,
		1.35e65,
		2.96e65,
		6.16e65,
		1.28e66,
		2.68e66,
		5.58e66,
		1.16e67,
		2.55e67,
		5.31e67,
		1.11e68,
		2.31e68,
		4.81e68,
		1e69,
		2.19e69,
		4.57e69,
		9.53e69,
		1.99e70,
		4.14e70,
		8.63e70,
		1.89e71,
		3.94e71,
		8.2e71,
		1.71e72,
		3.56e72,
		7.43e72,
		1.63e73,
		3.39e73,
		1.41e74,
		2.94e74,
		6.13e74,
		1.28e75,
		2.8e75,
		5.83e75,
		1.21e76,
		2.53e76,
		5.27e76,
		1.1e77,
		2.41e77,
		5.01e77,
		1.04e78,
		2.18e78,
		4.54e78,
		9.45e78,
		2.07e79,
		4.31e79,
		8.98e79,
		1.87e80,
		3.9e80,
		8.12e80,
		1.78e81,
		3.7e81,
		7.72e81,
		1.61e82,
		3.35e82,
		6.98e82,
		1.53e83,
		3.18e83,
		6.63e83,
		1.38e84,
		2.88e84,
		6e84,
		1.31e85,
		2.73e85,
		5.69e85,
		1.19e86,
		2.47e86,
		5.15e86,
		1.13e87,
		2.35e87,
		4.89e87,
		1.02e88,
		2.12e88,
		4.42e88,
		9.67e88,
		2.02e89,
		4.2e89,
		8.75e89,
		1.82e90,
		3.8e90,
		8.3e90,
		1.73e91,
		3.6e91,
		7.51e91,
		1.56e92,
		3.26e92,
		7.13e92,
		1.48e93,
		3.09e93,
		6.44e93,
		1.34e94,
		2.8e94,
		6.11e94,
		1.27e95,
		2.65e95,
		5.53e95,
		1.15e96,
		2.4e96,
		5.25e96,
	];

	if(!va) {
		va = vd[game.global.world];
	} else if (va < 71e2) {
		va = vd[va];
	}

	const cvf = {
		X: (combat_values.hp + combat_values.block),
		H: (combat_values.hp * 4 + combat_values.block / 2),
		D: (combat_values.hp / 2 + combat_values.block / 2),
		B: (combat_values.hp / 2 + combat_values.block * 4),
	};

	const uptime = {
		X: va*1.5 > combat_values.block ? combat_values.hp/(va*1.5 - combat_values.block)*5/3 : 1000,
		H: va*3 > combat_values.block ? combat_values.hp/(va*1.5 - combat_values.block/2)*20/3 : 1000,
		D: va*3 > combat_values.block ? combat_values.hp/(va*1.5 - combat_values.block/2)*5/6 : 1000,
		B: va*3 > combat_values.block*8 ? combat_values.hp/(va*1.5 - combat_values.block*4)*5/6 : 1000,
	};

	let status = "Imposible";
	if(cvf.D > va && game.upgrades.Dominance.done) {
		status = "D/S: Max Damage/Loot (" + Math.floor(uptime.D) + "%)";
	} else if(cvf.X > va) {
		status = "X: Normal (" + Math.floor(uptime.X) + "%)";
	} else if(cvf.B > va && game.upgrades.Barrier.done) {
		status = "B: Block to survive (" + Math.floor(uptime.B) + "%)";
	} else if(cvf.H > va && game.upgrades.Formations.done) {
		status = "H: Health to survive (" + Math.floor(uptime.H) + "%)";
	} else {
		let geneticists = 0;
		let gen_helth = game.upgrades.Formations.done ? cvf.H : cvf.X;
		while(gen_helth <= va && geneticists < 5e3)
		{
			geneticists++;
			gen_helth *= 1.01;
		}
		if(gen_helth > va) {
			status = "Imposible, Employ " + geneticists + " geneticists.";
		}
	}

	return {
		status: status,
		X: prettify(cvf.X) + " @ " + prettify(cvf.X / va),
		H: prettify(cvf.H) + " @ " + prettify(cvf.H / va),
		D: prettify(cvf.D) + " @ " + prettify(cvf.D / va),
		B: prettify(cvf.B) + " @ " + prettify(cvf.B / va),
		BH: prettify(combat_values.block / combat_values.hp),
		BP: prettify(combat_values.block / va),
		VMin: prettify(va),
		VMax: prettify(va*1.5),
	};
};

worldmap = (world, level, fast_only) => {
	if(!world)
	{
		world = game.global.world;
		level = 100;
	} else if (typeof level === "undefined") {
		level = 100;
	}

	let attack = 50 * Math.sqrt(world) * Math.pow(3.27, world / 2) - 10;
	if(world === 1)
	{
		attack *= 0.00074375 * (80 + 3 * level);
	}
	else if(world === 2)
	{
		attack *= 0.00017 * (800 + 17 * level);
	}
	else if(world < 60)
	{
		attack *= 0.00085 * (375 + 7 * level);
	}
	else
	{
		attack *= 0.001 * (400 + 9 * level) * Math.pow(1.15, world - 59);
	}

	if(fast_only) {
		// Snimp
		attack *= 1.05;
	} else if(level === 100) {
		// Omnipotrimp, Improbability, Blimp
		attack *= 1.2;
	} else {
		// Penguimp
		attack *= 1.1;
	}

	const cvf = {
		X: (combat_values.hp + combat_values.block),
		H: (combat_values.hp * 4 + combat_values.block / 2),
		D: (combat_values.hp / 2 + combat_values.block / 2),
		B: (combat_values.hp / 2 + combat_values.block * 4),
	};

	let status = "Imposible";
	if(cvf.D > attack && game.upgrades.Dominance.done) {
		status = "D: Max Damage";
	} else if(cvf.X > attack) {
		status = "X: Normal";
	} else if(cvf.B > attack && game.upgrades.Barrier.done) {
		status = "B: Block to survive";
	} else if(cvf.H > attack && game.upgrades.Formations.done) {
		status = "H: Health to survive";
	} else {
		let geneticists = 0;
		let gen_helth = game.upgrades.Formations.done ? cvf.H : cvf.X;
		while(gen_helth <= attack && geneticists < 5e3)
		{
			geneticists++;
			gen_helth *= 1.01;
		}
		if(gen_helth > attack) {
			status = "Imposible, Employ " + geneticists + " geneticists.";
		}
	}

	return {
		status: status,
		X: prettify(cvf.X) + " @ " + prettify(cvf.X / attack),
		H: prettify(cvf.H) + " @ " + prettify(cvf.H / attack),
		D: prettify(cvf.D) + " @ " + prettify(cvf.D / attack),
		B: prettify(cvf.B) + " @ " + prettify(cvf.B / attack),
		BH: prettify(combat_values.block / combat_values.hp),
		V: prettify(attack),
	};
};
