const gulp = require("gulp");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify-es").default;
const rename = require("gulp-rename");

gulp.task("default", ["uglify", "all"]);

gulp.task("uglify", () =>
	gulp.src(["*.js"])
		.pipe(gulp.dest("build"))
		.pipe(uglify())
		.pipe(rename({suffix: ".min"}))
		.pipe(gulp.dest("build")),
);

gulp.task("all", () =>
	gulp.src(
		[
			"config.js",
			"prettify.js",
			"void_map.js",
			"save.js",
			"formation.js",
			"move_jobs.js",
			"infobox.js",
			"mastery.js",
			"Essence.js",
			"puggan_auto.user.js",
		],
	)
		.pipe(concat("all.js"))
		.pipe(gulp.dest("build"))
		.pipe(uglify())
		.pipe(rename({suffix: ".min"}))
		.pipe(gulp.dest("build"))
		.pipe(gulp.dest(".")),
);
