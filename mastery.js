window.updateTalentNumbers = () => {
	var mainEssenceElem = document.getElementById("essenceOwned");
	var nextCostElem = document.getElementById("talentsNextCost");
	var talentsCostElem = document.getElementById("talentsCost");
	var alertElem = document.getElementById("talentsAlert");
	var countElem = document.getElementById("talentsEssenceTotal");
	//Check primary elements, update
	if (mainEssenceElem == null || nextCostElem == null) {return;}

	var nextCost = getNextTalentCost();
	mainEssenceElem.innerHTML = prettify(game.global.essence);
	if (nextCost === -1){
		talentsCostElem.style.display = "none";
		alertElem.innerHTML = "";
		countElem.innerHTML = "";
		return;
	}

	talentsCostElem.style.display = "block";
	nextCostElem.innerHTML = prettify(nextCost);
	//Check setting elements, update
	if (alertElem == null || countElem == null) { return; }
	if ((game.options.menu.masteryTab.enabled === 1 || game.options.menu.masteryTab.enabled === 3) && nextCost <= game.global.essence){
		alertElem.innerHTML = "!";
		countElem.innerHTML = (game.options.menu.masteryTab.enabled >= 2) ? " (+" + prettify(game.global.essence - nextCost) + ")" : "";
		return;
	}
	alertElem.innerHTML = "";
	countElem.innerHTML = (game.options.menu.masteryTab.enabled >= 2) ? " (" + prettify(game.global.essence - nextCost) + ")" : "";
};
