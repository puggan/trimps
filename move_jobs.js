window.move_jobs = (job, count, from_job) => {
	const amount = game.global.buyAmt;
	const job_fire_mode = game.global.firing;
	let employ_count = 0;

	const real_job_name = job_name => {
		switch(job_name.substr(0,1).toLowerCase())
		{
			case "m":
				return "Miner";

			case "l":
				return "Lumberjack";

			case "f":
				return "Farmer";

			case "s":
				return "Scientist";
		}
		return job_name;
	};

	job = real_job_name(job);

	if(from_job)
	{
		from_job = real_job_name(from_job);
		if(!game.jobs[from_job].owned)
		{
			return;
		}

		const fire_count = Math.min(game.jobs[from_job].owned, count ? count : game.jobs[from_job].owned);
		if(fire_count > 0)
		{
			employ_count += fire_count;
			game.global.buyAmt = fire_count;
			game.global.firing = true;
			buyJob(from_job, false, true);
		}
		else
		{
			return;
		}
	}
	else
	{
		const jobs = ["Miner", "Lumberjack", "Farmer"];
		for(const job_index in jobs)
		{
			const job_name = jobs[job_index];
			if(job_name === job)
			{
				continue;
			}
			const fire_count = Math.min(game.jobs[job_name].owned, count ? count : game.jobs[job_name].owned);
			employ_count += fire_count;
			game.global.buyAmt = fire_count;
			game.global.firing = true;
			buyJob(job_name, false, true);
		}
	}

	if(job && game.jobs[job])
	{
		employ_count = Math.max(employ_count, count ? count : employ_count);
		game.global.firing = false;
		game.global.buyAmt = employ_count;
		buyJob(job, false, true);
	}

	game.global.firing = job_fire_mode;
	game.global.buyAmt = amount;
};
