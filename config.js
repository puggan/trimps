/// <reference path="./config.d.ts" />

default_config = () => ({
	auto_employ: true,
	auto_fight: true,
	auto_fight_bonus: 1/20,
	auto_fight_farm: 50,
	auto_portal: false,
	geneticist_min_time: 30,
	gigastation_delta: 5,
	gigastation_init: 40,
	info_rows: {
		attack: 0,
		attack_speed: 1,
		enemy: 0,
		equipment: 3,
		health: 4,
		map_status: 2,
	},
	max_build_queue: 2,
	max_equipment: 10,
	max_equipment_shield: 15,
	max_explorer: 1000,
	max_houses: 100,
	max_map_equipment: 2,
	max_nursery: 500,
	nursery_max_time: 31, //30.3,
	potency_max_time: 33,
	prefered_stance: "S",
	prio_gold_battle: false,
	prio_gym_until: 100,
	prio_shield_until: 5,
	spire_wait: true,
	storage_min_count: 20,
	storage_min_time: 15*60,
	void_map_level: 201,
});

if(!window.puggan_auto_trimps) {
	puggan_auto_trimps = {
		config: default_config(),
		info: () => {},
		next_portal_config: default_config(),
		timers: {
			config: 0,
			formation: 0,
			play: 0,
			save: 0,
		},
	};
} else {
	puggan_auto_trimps.next_portal_config = Object.assign({}, default_config(), puggan_auto_trimps.next_portal_config);
	puggan_auto_trimps.config = Object.assign({}, puggan_auto_trimps.next_portal_config, puggan_auto_trimps.config);
}

if(puggan_auto_trimps.timers.config) {
	clearTimeout(puggan_auto_trimps.timers.config);
}
puggan_auto_trimps.timers.config = setInterval(() => {
	if(puggan_auto_trimps.config.portal && puggan_auto_trimps.config.portal === game.global.totalPortals) {
		return;
	}
	puggan_auto_trimps.config = Object.assign({}, puggan_auto_trimps.next_portal_config);
	puggan_auto_trimps.config.portal = game.global.totalPortals;
}, 2000);
